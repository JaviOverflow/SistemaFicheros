#!/bin/bash

nbloques=100000
nombreSF=d

echo -e "\n *** go.sh (Makefile: make all) -- Recompilamos si se ha modificado algún src"
make all
rm -r *.dSYM

echo -e "\n *** go.sh (mi_mkfs) -- Creamos el SF con $nbloques bloques" 
read
./mi_mkfs $nombreSF $nbloques

echo -e "\n *** go.sh (leer_SF) -- Mostramos el estado del SF"
read
./leer_SF $nombreSF

echo -e "\n *** go.sh (mi_touch) -- Creamos un fichero '/fichero1'"
read
./mi_touch $nombreSF 7 "/fichero1"

echo -e "\n *** go.sh (mi_creat) -- Creamos un directorio '/dir1/'"
read
./mi_mkdir $nombreSF 7 "/dir1/"

echo -e "\n *** go.sh (mi_ls) -- Listamos la raíz"
read
./mi_ls $nombreSF "/"

echo -e "\n *** go.sh (mi_ln) -- Linkamos /fichero1 a /copia"
read
./mi_ln $nombreSF "/fichero1" "/copia"

echo -e "\n *** go.sh (mi_ls) -- Listamos la raíz"
read
./mi_ls $nombreSF "/"

echo -e "\n *** go.sh (mi_stat) -- Comprobamos nlinks = 2 en '/fichero1'"
read
./mi_stat $nombreSF "/fichero1"

echo -e "\n *** go.sh (leer_SF) -- Mostramos el estado del SF"
read
./leer_SF $nombreSF

echo -e "\n *** go.sh (mi_escribir) -- Escribimos en el fichero '/copia'"
read
./mi_escribir $nombreSF "/copia" 5000

echo -e "\n *** go.sh (mi_cat) -- Mostramos el contenido del fichero 'fichero1'"
read
./mi_cat $nombreSF "/fichero1"

echo -e "\n *** go.sh (mi_ls) -- Listamos la raíz"
read
./mi_ls $nombreSF "/"

echo -e "\n *** go.sh (mi_rm) -- Borramos el fichero /fichero1"
read
./mi_rm $nombreSF "/fichero1"

echo -e "\n *** go.sh (mi_stat) -- Comprobamos nlinks = 1 en /copia"
read
./mi_stat $nombreSF "/copia"

echo -e "\n *** go.sh (mi_creat) -- Creamos un directorio en dir1"
read
./mi_mkdir $nombreSF 7 "/dir1/dir12/"

echo -e "\n *** go.sh (mi_rmdir) -- Intentamos borrar el directorio /dir1"
read
./mi_rmdir $nombreSF "/dir1/"

echo -e "\n *** go.sh (mi_ls) -- Listamos la raíz"
read
./mi_ls $nombreSF "/"

echo -e "\n *** go.sh (leer_SF) -- Mostramos el estado del SF"
read
./leer_SF $nombreSF

echo -e "\n *** go.sh (mi_mkdir) -- Creamos el directorio padre de los 17 dirs"
read
./mi_mkdir $nombreSF 7 "/dir17/"

echo -e "\n *** go.sh (mi_mkdir) -- Creamos los 17 directorios" 
read
for i in {1..17}
do
    ./mi_mkdir $nombreSF 7 "/dir17/$i/"
done

echo -e "\n *** go.sh (mi_ls) -- Listamos la raíz"
read
./mi_ls $nombreSF "/dir17/"

echo -e "\n *** go.sh (mi_stat) -- Comprobamos nlinks = 1 en /copia"
read
./mi_stat $nombreSF "/dir17"

echo -e "\n *** go.sh (mi_rmdir) -- Borramos un directorio en dir17"
read
./mi_rmdir $nombreSF "/dir17/1/"

echo -e "\n *** go.sh (mi_ls) -- Listamos la raíz"
read
./mi_ls $nombreSF "/dir17/"

echo -e "\n *** go.sh (mi_stat) -- Comprobamos nlinks = 1 en /copia"
read
./mi_stat $nombreSF "/dir17"

#echo -e "\n *** go.sh (mi_

#echo -e "\n *** go.sh (mi_
