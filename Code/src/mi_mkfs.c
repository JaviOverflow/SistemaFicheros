#include "lib/ficheros_basico.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Programa que crea e inicializa el fichero <arg1> con un número de bloques
    de memoria <arg2>. 
NOTA: El tamaño del bloque de memoria se encuentra definido en la cabecera
    bloques.h */
int main(int argc, char **argv){
    int i, nbloques;
    unsigned char bloque[BLOCKSIZE]; // Buffer para inicializar el fichero
    int e; // Manejo de errores

    // Comprobamos los argumentos
    if (argc != 3) {
        fprintf(stderr, "Se deben introducir dos y solo dos parámetros: la ruta\
            del fichero y el número de bloques de memoria que contendrá\n");
        exit(EXIT_FAILURE);
    }

    // Montamos nuestro sistema de ficheros
    bmount(argv[1]);

    // Obtenemos el número de bloques de los argumentos
    nbloques = atoi(argv[2]);

    // Inicializamos nuestro buffer a 0
    memset(bloque, 0, BLOCKSIZE);

    // Inicializamos el sistema de ficheros
    for ( i=0 ; i<nbloques ; i++){
        bwrite(i, bloque);
    }

    // Inicializamos el superbloque, el mapa de bits y el array de inodos
    initSB(nbloques, nbloques/4); initMB(); initAI();

    // Creamos el directorio raíz
    if (reservar_inodo('d',7) < 0) {
        fprintf(stderr, "No se ha podido crear el directorio raíz\n");
    }

    // Cerramos el fichero
    bumount();
}
