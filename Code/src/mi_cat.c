#include "lib/directorios.h"

int main(int argc, char **argv){
    int bufferSize = 4096;
    char buffer[bufferSize], string[128];
    unsigned int offset, leido;
    char *ruta;

    // Comprobamos el número de argumentos
    if (argc != 3) {
        fprintf(stderr, "Uso: mi_cat DISCO RUTA\n");
        exit(EXIT_FAILURE);
    }

    // Comprobamos que la ruta corresponda a un fichero normal
    ruta = argv[2];
    if (ruta[strlen(ruta) - 1] == '/') {
        fprintf(stderr, "ERROR: Debes introducir la ruta de un fichero\n");
        exit(EXIT_FAILURE);
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    // Obtenemos la información
    offset = 0;
    leido = 0;
    memset(buffer, 0, bufferSize);

    while ((leido = mi_read(argv[2], buffer, offset, bufferSize)) > 0){
        write(1, buffer, leido);
        offset += leido;
        memset(buffer, 0, bufferSize);
    }

    //sprintf(string, "Bytes leídos %d\n", offset);
    //write(2, string, strlen(string));
    
    switch (errno) {
        case ENOENT:
            fprintf(stderr, "ERROR: El fichero no existe\n");
            break;
        case EACCES:
            fprintf(stderr, "ERROR: No hay permisos de lectura\n");
            break;
    }

    bumount();
}
