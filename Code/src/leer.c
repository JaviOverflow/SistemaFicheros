#include "lib/ficheros.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>


int main(int argc, char **argv){
    int bufferSize = 10000;
    char buffer[bufferSize], string[128];
    int ninodo, offset, leido;
    
    // Comprobamos los argumentos
    if (argc != 3) {
        fprintf(stderr, "Se deben introducir dos y solo dos parámetros: la ruta del fichero que "
            "contiene el sistema de ficheros y el número de inodo a leer\n");
        exit(EXIT_FAILURE);
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    ninodo = atoi(argv[2]);
    offset = 0;
    leido = 0;
    memset(buffer, 0, bufferSize);

    while ((leido = mi_read_f(ninodo, buffer, offset, bufferSize)) > 0){
        write(1, buffer, leido);
        offset += leido;
        memset(buffer, 0, bufferSize);
    }

    if (errno == EACCES) {
        fprintf(stderr, "El inodo %d no tiene permiso de lectura\n", ninodo);
    } else {
        sprintf(string, "Bytes leídos %d\n", offset);
        write(2, string, strlen(string));
    }

    // Cerramos el fichero
    bumount();

    exit(EXIT_SUCCESS);
}
