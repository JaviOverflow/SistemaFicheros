#include "lib/ficheros.h"
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv){

    int ninodo, i, e;
    unsigned int offsets[4] = {5120, 256000, 30720000, 71680000};
    char buffer1[30], buffer2[3400];

    // Comprobamos los argumentos
    if (argc != 2) {
        fprintf(stderr, "Se debe introducir la ruta del fichero que contiene el sistema de \
            ficheros\n");
        exit(EXIT_FAILURE);
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    // Escribimos en todos los offsets en el primer inodo (será el 1)
    ninodo = reservar_inodo('f', 6);
    printf("El número de inodo es %d\n", ninodo);
    strcpy(buffer1, "La casa de pepito\0");
    printf("La longitud de buffer1 es %zu\n", strlen(buffer1));

    // Le quitamos los permisos
    mi_chmod_f(ninodo, 0);

    for (i = 0; i < 4; i++){
        mi_write_f(ninodo, buffer1, offsets[i], strlen(buffer1));
    }
    if (errno == EACCES){
        fprintf(stderr, "El inodo %d no tiene permisos de escritura\n", ninodo);
        errno = 0;
    }

    // Reservamos otro inodo y escribimos en su offset 200, un buffer de 3400 bytes
    ninodo = reservar_inodo('f', 6);
    printf("El número de inodo es %d\n", ninodo);
    memset(buffer2, 90, sizeof(buffer2));
    printf("La longitud de buffer2 es %zu\n", strlen(buffer2));

    mi_write_f(ninodo, buffer2, 200, strlen(buffer2));
    if (errno == EACCES){
        fprintf(stderr, "El inodo %d no tiene permisos de escritura\n", ninodo);
        errno = 0;
    }

    // Posteriormente lo truncamos a 1500 bytes
    mi_truncar_f(ninodo, 1500);
    if (errno == EACCES){
        fprintf(stderr, "No se pudo truncar correctamente\n");
        errno = 0;
    }

    // Cerramos el fichero
    bumount();

    exit(EXIT_SUCCESS);
}
