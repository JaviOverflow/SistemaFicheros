#include "lib/directorios.h"

int main(int argc, char **argv){
    int bufferSize = 4096;
    char buffer[bufferSize], string[128];
    unsigned int offset, leido;
    char *ruta;

    // Comprobamos el número de argumentos
    if (argc != 4) {
        fprintf(stderr, "Uso: mi_escribir DISCO RUTA OFFSET\n");
        exit(EXIT_FAILURE);
    }

    // Comprobamos que la ruta corresponda a un fichero normal
    ruta = argv[2];
    if (ruta[strlen(ruta) - 1] == '/') {
        fprintf(stderr, "ERROR: Debes introducir la ruta de un fichero normal\n");
        exit(EXIT_FAILURE);
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    // Obtenemos la información
    offset = atoi(argv[3]);
    leido = 0;
    memset(buffer, 0, bufferSize);

    while ((leido = read(0, buffer, bufferSize)) > 0){
        mi_write(argv[2], buffer, offset, leido);
        if (errno == EOVERFLOW) break;
        offset += leido;
        memset(buffer, 0, bufferSize);
    }

    // Comprobamos si ha ido satisfactoriamente
    switch (errno) {
        case ENOENT:
            fprintf(stderr, "ERROR: El fichero no existe\n");
            break;
        case EACCES:
            fprintf(stderr, "ERROR: No hay permisos de escritura\n");
            break;
        case EOVERFLOW:
            fprintf(stderr, "ERROR: Overflow. Se ha excedido el límite del fichero permitido\n");
            break;
    }
    bumount();
}
