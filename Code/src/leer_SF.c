#include "lib/bloques.h"
#include "lib/ficheros_basico.h"
#include <stdio.h>

int main(int argc, char **argv){
    superbloque_t sb;
    inodo_t inodo;
    char xtime[80];
	int i, nbloque, ninodo, numInodosOcupados, bit, numBloquesOcupados;
    //inodo_t bloqueInodos[BLOCKSIZE/T_INODO];
    //unsigned char bloqueMB[BLOCKSIZE];

    // Comprobamos los argumentos
    if (argc != 2) {
        fprintf(stderr, "Se deben introducir dos y solo dos parámetros: la ruta"
            "del fichero y el número de bloques de memoria que contendrá\n");
        exit(EXIT_FAILURE);
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    // Leemos el superbloque
    bread(POS_SB, &sb);

    printf("1.- CAMPOS DEL SUPERBLOQUE\n");
    printf("\tPosición del primer/último bloque MB: %d / %d\n", sb.posPrimerBloqueMB, sb.posUltimoBloqueMB);
    printf("\tPosición del primer/último bloque AI: %d / %d\n", sb.posPrimerBloqueAI, sb.posUltimoBloqueAI);
    printf("\tPosición del primer/último bloque de Datos: %d / %d\n", sb.posPrimerBloqueDatos, sb.posUltimoBloqueDatos);
    printf("\tPosición del inodo raíz: %d \n", sb.posInodoRaiz);
    printf("\tPosición del primer inodo libre: %d \n", sb.posPrimerInodoLibre);
    printf("\tCantidad inodos libres: %d \n", sb.cantInodosLibres);
    printf("\tCantidad bloques libres: %d \n", sb.cantBloquesLibres);
    printf("\tCantidad bloques total: %d \n", sb.totBloques);
    printf("\tCantidad inodos total: %d \n", sb.totInodos);
    printf("\tTamaño del superbloque_t: %zu \n", sizeof(superbloque_t));

    printf("2.- TAMAÑO EN BLOQUES DEL...\n");
    printf("\tMapa de bits: %d\n", sb.posUltimoBloqueMB + 1 - sb.posPrimerBloqueMB);
    printf("\tArray de inodos: %d\n", sb.posUltimoBloqueAI + 1 - sb.posPrimerBloqueAI);

    printf("3.- TAMAÑO INODO: %d\n", T_INODO);

    printf("4.- INFORMACIÓN DE INODOS OCUPADOS:\n");
    for (ninodo = 0, numInodosOcupados = 0; (ninodo < sb.totInodos) && 
        (numInodosOcupados < (sb.totInodos - sb.cantInodosLibres)); ninodo++){

        inodo = leer_inodo(ninodo);
        if (inodo.tipo != 'l'){
            ++numInodosOcupados;

            printf("\tInodo número %d\n", ninodo);
            printf("\t\tTipo: %c\n", inodo.tipo);
            printf("\t\tPermisos: %d\n", inodo.permisos);
            printf("\t\tnlinks: %d\n", inodo.nlinks);
            printf("\t\tTamaño en bytes lógicos: %d\n", inodo.tamEnBytesLog);
            printf("\t\tNúmero de bloques ocupados: %d\n", inodo.numBloquesOcupados);
            strftime (xtime, sizeof(xtime), "%a %Y-%m-%d %H:%M:%S", localtime(&inodo.atime));
            printf("\t\tatime: %s\n", xtime);
            strftime (xtime, sizeof(xtime), "%a %Y-%m-%d %H:%M:%S", localtime(&inodo.mtime));
            printf("\t\tmtime: %s\n", xtime);
            strftime (xtime, sizeof(xtime), "%a %Y-%m-%d %H:%M:%S", localtime(&inodo.ctime));
            printf("\t\tctime: %s\n", xtime);
        }
    }

    printf("5.- LISTA DE BLOQUES OCUPADOS:\n");
    for (nbloque = sb.posPrimerBloqueDatos, numBloquesOcupados = sb.posPrimerBloqueDatos; (nbloque < sb.totBloques) && 
        (numBloquesOcupados < (sb.totBloques - sb.cantBloquesLibres)); nbloque++){

        bit = leer_bit (nbloque);
        if (bit){
            ++numBloquesOcupados;

            printf(" %d ", nbloque);
        }
    }
    puts("");

    // Desmontamos el sistema de ficheros 
    bumount();

    exit(EXIT_SUCCESS);
}
