#include "lib/directorios.h"

int main(int argc, char **argv){
    // Comprobamos el número de argumentos
    if (argc != 4) {
        fprintf(stderr, "Uso: mi_ln DISCO RUTA_FICHERO RUTA_ENLACE\n");
        exit(EXIT_FAILURE);
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    // Creamos el enlace
    mi_link(argv[2], argv[3]);

    // Comprobamos si ha ido satisfactoriamente
    switch (errno) {
        case EISDIR:
            fprintf(stderr, "ERROR: Debes especificar un fichero normal, no un directorio\n");
            break;
        case ENOENT:
            fprintf(stderr, "ERROR: No existe el fichero origen\n");
            break;
        case EACCES:
            fprintf(stderr, "ERROR: El fichero origen no tiene permisos de lectura\n");
            break;
    }
    bumount();
}
