#include "lib/directorios.h"
#include "registro.h"
#include <signal.h>
#include <sys/wait.h>

#define POSMAX 500000
#define NHIJOS 50

static int acabados;
static char dirSim[60], sf[64];

void reaper(){
    while(wait3(NULL, WNOHANG, NULL) > 0) {
        acabados++;
    }
}

void hijo(){
    char dirHijo[60];
    char fichHijo[60];
    struct _registro registro;
    int i;

    sprintf(dirHijo, "%sproceso_%d/", dirSim, getpid());
    sprintf(fichHijo, "%sprueba.dat", dirHijo);

    mi_creat(dirHijo, 7);
    mi_creat(fichHijo, 7);

    registro.pid = getpid();
    registro.nEscritura = 0;
    srand(time(NULL) + getpid());

    for (i = 0; i < 50; i++) {
        registro.fecha = time(NULL);
        registro.nEscritura += 1;
        registro.posicion = rand() % POSMAX;

        if (mi_write(fichHijo, &registro, registro.posicion * sizeof(struct _registro), 
                sizeof(struct _registro)) < 0) { fprintf(stderr, "Error al escribir el registro en el proceso hijo\n");
            exit(EXIT_FAILURE);
        }

        usleep(50000); // 0.05 seg
    }

    exit(EXIT_SUCCESS);
}

int main(int argc, char **argv){
    time_t *now;
    int i;

    // Comprobamos el número de argumentos
    if (argc != 2) {
        fprintf(stderr, "Uso: simulacion DISCO\n");
        exit(EXIT_FAILURE);
    } else {
        strcpy(sf, argv[1]);
    }

    // Creamos el directorio de simulación a partir de la fecha epochh actual
    time(now);
    strftime(dirSim, sizeof(dirSim), "/simul_%Y%m%d%H%M%S/", localtime(now));
    printf("Directorio de simulación: %s\n", dirSim);

    bmount(sf);

    mi_creat(dirSim, 7);

    // Fijamos el enterrador de los hijos zombies
    signal(SIGCHLD, reaper);
    
    for (i = 0; i < NHIJOS; i++) {
        switch (fork()) {
            case 0:
                hijo();
                break;
            case -1:
                fprintf(stderr, "No se pudo crear uno de los hijos\n");
                exit(EXIT_FAILURE);
        }
        usleep(200000); // 0.2 segundos
    }

    // Esperamos a que acaben los hijos
    while (acabados < NHIJOS) pause();

    bumount(); // Volveremos a montar el sistema de ficheros dentro de cada hijo
 
    exit(EXIT_SUCCESS);
}
