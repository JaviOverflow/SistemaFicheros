#include "lib/directorios.h"

int main(int argc, char **argv){
    int permisos; char aux;

    // Comprobamos el número de argumentos
    if (argc != 4) {
        fprintf(stderr, "Uso: mi_chmod DISCO PERMISOS RUTA\n");
        exit(EXIT_FAILURE);
    }

    // Obtenemos los permisos si se han introducido
    permisos = *(argv[2]) - '0';
    if (! (strlen(argv[2]) == 1 && permisos >= 0 && permisos <= 7)) {
        permisos = ((strchr(argv[2], 'r') != NULL) ? 4 : 0) +
                   ((strchr(argv[2], 'w') != NULL) ? 2 : 0) + 
                   ((strchr(argv[2], 'x') != NULL) ? 1 : 0);
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    // Cambiamos los permisos
    mi_chmod(argv[3], (unsigned char) permisos);

    // Comprobamos si ha ido satisfactoriamente
    switch (errno) {
        case ENOENT:
            fprintf(stderr, "ERROR: No existe el fichero/directorio\n");
            break;
        case EACCES:
            fprintf(stderr, "ERROR: No hay permisos suficientes en el directorio padre\n");
            break;
    }
    bumount();
}
