#include "lib/directorios.h"

int main(int argc, char **argv){
    int permisos;
    char *ruta;

    // Comprobamos el número de argumentos
    if (argc != 3 && argc != 4) {
        fprintf(stderr, "Uso: mi_touch DISCO [PERMISOS] RUTA\n");
        exit(EXIT_FAILURE);
    }

    // Comprobamos que la ruta corresponde a un fichero
    ruta = argv[argc - 1];
    if (ruta[strlen(ruta) - 1] == '/') {
        fprintf(stderr, "ERROR: La ruta no corresponde a un fichero\n");
        exit(EXIT_FAILURE);
    }

    // Obtenemos los permisos si se han introducido
    if (argc == 4) {
        permisos = *(argv[2]) - '0';
        if (! (strlen(argv[2]) == 1 && permisos >= 0 && permisos <= 7)) {
            permisos = ((strchr(argv[2], 'r') != NULL) ? 4 : 0) +
                       ((strchr(argv[2], 'w') != NULL) ? 2 : 0) + 
                       ((strchr(argv[2], 'x') != NULL) ? 1 : 0);
        }
    } else {
        permisos = 6; // Permisos por defecto
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    // Creamos el fichero
    mi_creat(ruta, (unsigned char) permisos);

    // Comprobamos si ha ido satisfactoriamente
    switch (errno) {
        case EACCES:
            fprintf(stderr, "ERROR: No hay permisos suficientes en el directorio padre\n");
            break;
        case ENOENT:
            fprintf(stderr, "ERROR: No existen los directorios intermedios\n");
            break;
        case ENOTDIR:
            fprintf(stderr, "ERROR: Debes proporcionar la ruta de un directorio para el nuevo fichero\n");
            break;
    }
    bumount();
}
