#include "lib/directorios.h"

int main(int argc, char **argv){
    struct _stat info;
    char xtime[80];

    // Comprobamos el número de argumentos
    if (argc != 3) {
        fprintf(stderr, "Uso: mi_stat DISCO RUTA\n");
        exit(EXIT_FAILURE);
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    // Obtenemos la información
    mi_stat(argv[2], &info);

    // Comprobamos si ha ido satisfactoriamente
    if (errno >= 0) {

        // Mostramos la información
        printf("Fichero: %s\n", argv[2]);

        printf("Tamaño: %d", info.tamEnBytesLog);
        printf("\tBloques: %d", info.numBloquesOcupados);
        printf("\tTipo: %s\n", (info.tipo == 'd') ? "Directorio" : "Fichero normal");

        printf("Acceso: (%d/%c%c%c)", info.permisos,
            ((info.permisos & 4) ? 'r' : '-'),
            ((info.permisos & 2) ? 'w' : '-'),
            ((info.permisos & 1) ? 'x' : '-'));
        printf("\tnlinks: %d\n", info.nlinks);

        strftime (xtime, sizeof(xtime), "%a %Y-%m-%d %H:%M:%S", localtime(&info.atime));
        printf("Acceso: %s\n", xtime);
        strftime (xtime, sizeof(xtime), "%a %Y-%m-%d %H:%M:%S", localtime(&info.mtime));
        printf("Modificación: %s\n", xtime);
        strftime (xtime, sizeof(xtime), "%a %Y-%m-%d %H:%M:%S", localtime(&info.ctime));
        printf("Cambio: %s\n", xtime);

        bumount();
        exit(EXIT_SUCCESS);

    // En caso de error, notificamos al usuario
    } else {
        switch (errno) {
            case ENOENT:
                fprintf(stderr, "ERROR: El fichero no existe\n");
                break;
            case EACCES:
                fprintf(stderr, "ERROR: No hay permisos de lectura\n");
                break;
        }
        bumount();
        exit(EXIT_FAILURE);
    }
}
