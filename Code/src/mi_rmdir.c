#include "lib/directorios.h"

int main(int argc, char **argv){
    char *ruta;

    // Comprobamos el número de argumentos
    if (argc != 3) {
        fprintf(stderr, "Uso: mi_rmdir DISCO RUTA\n");
        exit(EXIT_FAILURE);
    }

    // Comprobamos que la ruta corresponde a un directorio
    ruta = argv[argc - 1];
    if (ruta[strlen(ruta) - 1] != '/') {
        fprintf(stderr, "ERROR: La ruta no corresponde a un directorio\n");
        exit(EXIT_FAILURE);
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    // Creamos el directorio y comprobamos si ha ido satisfactoriamente
    if (mi_unlink(argv[2]) >= 0) {
        bumount();
        exit(EXIT_SUCCESS);
    } else {
        switch (errno) {
            case ENOTEMPTY:
                fprintf(stderr, "ERROR: El directorio no está vacío\n");
                break;
            case EACCES:
                fprintf(stderr, "ERROR: No hay permisos suficientes en el directorio padre\n");
                break;
            case ENOENT:
                fprintf(stderr, "ERROR: No existe el directorio\n");
                break;
        }
        bumount();
        exit(EXIT_FAILURE);
    }
}
