#include "lib/directorios.h"
#include "registro.h"

#define N_ENTRADAS 50

struct _info {
    pid_t pid;
    unsigned int nRegistrosValidos;
    struct _registro primeraEscritura, ultimaEscritura;
    struct _registro menorPosicion, mayorPosicion;
};

void imprimirInfo(struct _info info, char *rutaInforme, int *offsetInfo);
void imprimirRegistro(struct _registro reg, char *rutaInforme, int *offsetInfo, const char *tit);

int main(int argc, char **argv){
    struct _stat st;
    char rutaInforme[LONG_ENTRADA*4], rutaDatos[LONG_ENTRADA*4];
    struct _entrada entradas[N_ENTRADAS];
    struct _info info;
    int nRegs = BLOCKSIZE/sizeof(struct _registro);
    struct _registro reg, regs[nRegs], mask[nRegs];
    int i, j, offset, offsetInfo = 0, leido;

    if (argc != 3) {
        fprintf(stderr, "Uso: verificacion DISCO DIRECTORIO_SIMULACION\n");
        exit(EXIT_FAILURE);
    }

    bmount(argv[1]);

    mi_stat(argv[2], &st);
    if (st.tamEnBytesLog/sizeof(struct _entrada) != N_ENTRADAS) {
        fprintf(stderr, "Error: No contiene 100 entradas\n");
        bumount(); exit(EXIT_FAILURE);
    }

    sprintf(rutaInforme, "%sinforme.txt", argv[2]);
    if (mi_creat(rutaInforme, 7) < 0) {
        fprintf(stderr, "Error: No se pudo crear el fichero informe.txt\n");
        bumount(); exit(EXIT_FAILURE);
    }

    mi_read(argv[2], entradas, 0, sizeof(entradas));
    
    for (i = 0; i < N_ENTRADAS; i++) {
        info.pid = atoi(strchr(entradas[i].nombre, '_') + 1);
        info.nRegistrosValidos = 0;
        sprintf(rutaDatos, "%s%s/prueba.dat", argv[2], entradas[i].nombre);

        leido = 0; offset = 0;
        //memset(regs, 0, sizeof(regs));
        //memset(mask, 0, sizeof(regs));
        while ((leido = mi_read(rutaDatos, regs, offset, sizeof(regs))) > 0){
            offset += leido;

            //if (! memcmp(regs, mask, sizeof(regs))) continue;

            for (j = 0; j < nRegs; j++) {

                if (regs[j].pid == info.pid) {
                    reg = regs[j];
                    info.nRegistrosValidos += 1;

                    if (info.nRegistrosValidos == 1) {
                        info.primeraEscritura = reg;
                        info.ultimaEscritura = reg;
                        info.menorPosicion = reg;
                        info.mayorPosicion = reg;
                    }

                    if (info.primeraEscritura.fecha > reg.fecha || 
                            (info.primeraEscritura.fecha == reg.fecha && 
                             info.primeraEscritura.nEscritura > reg.nEscritura)) {
                        info.primeraEscritura = reg;
                    }
                    if (info.ultimaEscritura.fecha < reg.fecha ||
                            (info.ultimaEscritura.fecha == reg.fecha && 
                             info.ultimaEscritura.nEscritura < reg.nEscritura)) {
                        info.ultimaEscritura = reg;
                    }
                    if (info.menorPosicion.posicion > reg.posicion) info.menorPosicion = reg;
                    if (info.mayorPosicion.posicion < reg.posicion) info.mayorPosicion = reg;
                }
                regs[j].pid = 0;
            }
            //memset(&regs, 0, sizeof(regs));
        }

        //offsetInfo += mi_write(rutaInforme, &info, offsetInfo, sizeof(struct _info));

        // Imprimimos en informe.txt los resultados
        imprimirInfo(info, rutaInforme, &offsetInfo);
    }

    bumount();
    exit(EXIT_SUCCESS);
}

void imprimirInfo(struct _info info, char *rutaInforme, int *offsetInfo) {
    char strInfo[256];

    sprintf(strInfo, "\nPROCESO PID: %d\n-- Registros válidos: %d\n", info.pid, info.nRegistrosValidos);
    *offsetInfo += mi_write(rutaInforme, strInfo, *offsetInfo, strlen(strInfo));

    imprimirRegistro(info.primeraEscritura, rutaInforme, offsetInfo, "-- Primera escritura: \n");

    imprimirRegistro(info.ultimaEscritura, rutaInforme, offsetInfo, "-- Última escritura: \n");

    imprimirRegistro(info.menorPosicion, rutaInforme, offsetInfo, "-- Menor posición: \n");

    imprimirRegistro(info.mayorPosicion, rutaInforme, offsetInfo, "-- Mayor posición: \n");

}

void imprimirRegistro(struct _registro reg, char *rutaInforme, int *offsetInfo, const char *tit) {
    char time[32], strInfo[256];

    strftime(time, sizeof(time), "%Y %m %d - %H:%M:%S", localtime(&reg.fecha));
    sprintf(strInfo, "%s---- Fecha %s\nPosicion: %d\tNúmero de escrituras: %d\n", 
            tit, time, reg.posicion, reg.nEscritura);
    
    *offsetInfo += mi_write(rutaInforme, strInfo, *offsetInfo, strlen(strInfo));
}
