#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>

#include "semaforo_mutex_posix.h"

static unsigned semCont = 0;

/* Ejemplo de creación e inicialización de semáforos POSIX para MUTEX con "semáforos con nombre" (named) */
sem_t *init_sem() {
    /* name debe ser un nombre de caracteres ascii que comienze con "/", p.e. "/mimutex" */
    sem_t *sem;

    sem = sem_open(SEM_NAME, O_CREAT, S_IRWXU, SEM_INIT_VALUE);
    if (sem == SEM_FAILED) {
        return NULL;
    }
    return sem;
}

void delete_sem() {
    sem_unlink(SEM_NAME);
}

void signal_sem(sem_t *sem) {
    if (semCont == 1) {
        sem_post(sem);
    }
    --semCont;
}

void wait_sem(sem_t *sem) {
    if (semCont == 0) {
        sem_wait(sem);
    }
    ++semCont;
}
