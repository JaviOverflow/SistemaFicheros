#include "bloques.h"


static int descriptor; // File descriptor del fichero que contiene el sistema de ficheros
static sem_t *mutex; // Semaforo estatico

/* Realiza un wait en el semáforo en el proceso que llama */
void mi_wait_sem() {
    wait_sem(mutex);
}

/* Realiza un signal en el semáforo en el proceso que llama */
void mi_signal_sem() {
    signal_sem(mutex);
}

/** 
*	Monta el sistema de ficheros. Internamente, abre el fichero que contiene el SF y guarda
*   su descriptor. Si no existe, se crea dicho fichero.
*
*	@param const char *camino : Ruta del fichero
*	@return int descriptor : Descriptor de fichero
*/
int bmount(const char *camino){
    descriptor = open(camino, O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);
    if (descriptor < 0) { fprintf(stderr, "Error grave\n"); exit(EXIT_FAILURE); }
    mutex = init_sem();
    return descriptor;
}


/** 
*	Desmonta el sistema de ficheros. Internamente, cierra el descriptor que apunta al fichero
*   que contiene el sistema de ficheros abierto.
*
*	@return 0
*/
int bumount(){
    if (close(descriptor) < 0) { fprintf(stderr, "Error grave\n"); exit(EXIT_FAILURE); }
    delete_sem();
    return 0;
}

/**
*   Función que guarda el contenido de un buffer <buf> en el bloque número <nbloque> del 
*   sistema de ficheros. 
*
*   @param unsigned int nbloque
*   @param const void *buf
*   @return int : número de bytes escritos
*/
int bwrite(unsigned int nbloque, const void *buf){
    int n; // Número de bytes escritos

    // Situamos el puntero
    if (lseek(descriptor, nbloque * BLOCKSIZE, SEEK_SET) < 0) {
        fprintf(stderr, "Error grave\n"); 
        exit(EXIT_FAILURE);
    }

    // Escribimos el buffer en el bloque
    n = write(descriptor, buf, BLOCKSIZE);
    if (n < 0) { fprintf(stderr, "Error grave\n"); exit(EXIT_FAILURE); }

    return n;
}
    
/**
*   Función que lee y guarda en el buffer <buf> el contenido del bloque de memoria número 
*   <nbloque> del sistema de ficheros. 
*
*   @param unsigned int nbloque
*   @param const void *buf
*   @return int : número de bytes leídos
*/
int bread(unsigned int nbloque, void *buf){
    int n; // Número de bytes leídos

    // Situamos el puntero
    if (lseek(descriptor, nbloque * BLOCKSIZE, SEEK_SET) < 0) {
        fprintf(stderr, "Error grave\n"); 
        exit(EXIT_FAILURE);
    }

    // Leemos el bloque en el buffer
    n = read(descriptor, buf, BLOCKSIZE);
    if (n < 0) { fprintf(stderr, "Error grave\n"); exit(EXIT_FAILURE); }

    return n;
}


