#include "ficheros_basico.h"

/* Tipo que define la metainformación de un fichero o directorio  */
struct _stat{
    unsigned char tipo; // Tipo (libre, directorio o fichero)
    unsigned char permisos; // Permisos (lectura y/o escritura y/o ejecución)
    time_t atime;   // Fecha y hora del último acceso a datos
    time_t mtime;   // Fecha y hora de la última modificación de datos
    time_t ctime;   // Fecha y hora de la última modificación del inodo
    unsigned int nlinks;    // Cantidad de enlaces de entradas en directorio
    unsigned int tamEnBytesLog; // Tamaño en bytes lógicos
    unsigned int numBloquesOcupados;    // Cantidad de bloques ocupados en la zona de datos
};

/**
*	Escribe 'nbytes' del contenido de un buffer de memoria 'buf_original' en el contenido de un 
*   inodo 'inodo' a partir del byte 'offset'
*
*	@param unsigned int ninodo : Indice del inodo en el AI
*	@param const void *buf_original : Buffer de memoria
*	@param unsigned int offset : Posición del primer byte del inodo a escribir
*	@param unsigned int nbytes : Número de bytes que se deben escribir a partir del offset
*
*	@return int bytes: Cantidad de bytes escritos
*/
int mi_write_f (unsigned int ninodo, const void *buf_original, unsigned int offset, unsigned int nbytes);

/**
*	Vuelca en un buffer 'buf_original' un número de bytes 'nbytes' de información que contiene un 
*   inodo 'ninodo' en la posición 'offset'
*
*	@param unsigned int ninodo : Indice del inodo en el AI
*	@param const void *buf_original : Buffer donde se volcarán los datos
*	@param unsigned int offset : Posición del primer byte del inodo a leer
*	@param unsigned int nbytes : Número de bytes que se deben leer a partir del offset
*
*	@return int bytes: Cantidad de bytes leídos.
*/
int mi_read_f (unsigned int ninodo, void *buf_original, unsigned int offset, unsigned int nbytes);

/**
*   Función que cambia los permisos de un inodo   
*
*   @param unsigned int ninodo : número de inodo 
*   @param unsigned char permisos : permisos actualizados
*   @return int : 0 (correcto) | ERROR_MI_CHMOD_F (error)
*/
int mi_chmod_f (unsigned int ninodo, unsigned char permisos);

/**
*   Función que trunca un inodo 'ninodo' a partir de una posición de bytes 'nbytes'
*
*   @param unsigned int ninodo : número de inodo
*   @param unsigned int nbytes : posición del byte a partir del cual se debe truncar
*   @return int : 0 (correcto) | ERROR_MI_TRUNCAR_F (error genérico) | 
*       ERROR_MI_TRUNCAR_F_PERMISO_ESCRITURA (el inodo no tiene permisos de escritura)
*/
int mi_truncar_f (unsigned int ninodo, unsigned int nbytes);

/**
*   Función que deposita en el contenido de 'p_stat' la metainformación del fichero/directorio
*       con índice 'ninodo'
*
*   @param unsigned int ninodo : número de inodo
*   @param struct _stat *p_stat : estructura donde se escribirá la metainformación
*   @return int : 0 (correcto)
*/
int mi_stat_f (unsigned int ninodo, struct _stat *p_stat);
