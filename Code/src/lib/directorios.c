#include "directorios.h"

struct _ultimaEntrada {
    char camino[512];
    unsigned int p_inodo;
};
struct _ultimaEntrada gUltimaEntradaLectura, gUltimaEntradaEscritura;

//////////////////////////// DECLARACIÓN DE FUNCIONES PRIVADAS ///////////////////////////////

/**
*   Función que dado un camino, deposita en 'inicial' el siguiente fichero/directorio y en 'final'
*       el resto del camino.
*
*   @param const char *camino
*   @param char *inicial: contendrá el nombre del siguiente directorio/fichero (sin '/').
*   @param char *final: contendrá lo mismo que 'camino' pero sin el directorio/fichero inicial
*/
void extraer_camino(const char *camino, char *inicial, char *final, unsigned char *tipo);

/**
*   Función que busca el inodo descrito por la ruta 'camino_parcial' y extrae su información de
*   localización dentro del sistema de ficheros.
*
*   @param const char *camino_parcial : ruta
*   @param unsigned int *p_inodo_dir : incialmente el número de inodo del directorio más alto en
*       la jerarquía descrita en 'camino_parcial'. Finalmente, contendrá el directorio padre del 
*       inodo que se busca.
*   @param unsigned int *p_inodo : contendrá el número del inodo que se busca
*   @param unsigned int *p_entrada : contendrá el número de entrada dentro del directorio padre
*   @param char reservar : 1 (si el inodo no existe y se debe reservar) | 0 (caso contrario)
*   @param unsigned char permisos : permisos del inodo en caso de que se tenga que crear
*
*   @return 0 (correcto) | -1 (error)
*
*   @errno  EACCES (permisos) 
*           EEXIST (reservar == true y ya existe) 
*           ENOENT (reservar == false y no existe)
*/
int buscar_entrada(const char *camino_parcial, unsigned int *p_inodo_dir, unsigned int *p_inodo,
    unsigned int *p_entrada, char reservar, unsigned char permisos);

/**
*   Función interna que traduce el nombre de un inodo 'nombre_entrada' en su número de inodo
*   'num_sig_inodo' y su número de entrada '@return' a partir del directorio padre 'p_inodo_dir'
*
*   @param unsigned int *p_inodo_dir : número inodo del directorio padre
*   @param char *nombre_entrada : nombre del inodo a buscar
*   @param unsigned int *num_sig_inodo : contendrá el número de inodo del inodo buscado
*
*   @return 0 (correcto) | -1 (error)
*
*   @errno  EEXIST (reservar == true y ya existe) 
*           ENOENT (reservar == false y no existe)
*/
int traducir_entrada(unsigned int num_dir_padre, char *nombre_entrada, unsigned int *num_sig_inodo, 
    char reservar, char tipo, unsigned char permisos);



//////////////////////////////////////// IMPLEMENTACIÓN ////////////////////////////////////////

/**
*   Función que dado un camino, deposita en 'inicial' el siguiente fichero/directorio y en 'final'
*       el resto del camino.
*
*   @param const char *camino
*   @param char *inicial: contendrá el nombre del siguiente directorio/fichero (sin '/').
*   @param char *final: contendrá lo mismo que 'camino' pero sin el directorio/fichero inicial
*/
void extraer_camino(const char *camino, char *inicial, char *final, unsigned char *tipo){
    char *sep;

    strcpy(inicial, camino + 1);
    sep = strchr(inicial, '/');

    if (sep) {
        strcpy(final, sep);
        *sep = 0;
        *tipo = 'd';
    } else {
        *final = 0;
        *tipo = 'f';
    }
}

/**
*   Función que busca el inodo descrito por la ruta 'camino_parcial' y extrae su información de
*   localización dentro del sistema de ficheros.
*
*   @param const char *camino_parcial : ruta
*   @param unsigned int *p_inodo_dir : incialmente el número de inodo del directorio más alto en
*       la jerarquía descrita en 'camino_parcial'. Finalmente, contendrá el directorio padre del 
*       inodo que se busca.
*   @param unsigned int *p_inodo : contendrá el número del inodo que se busca
*   @param unsigned int *p_entrada : contendrá el número de entrada dentro del directorio padre
*   @param char reservar : 1 (si el inodo no existe y se debe reservar) | 0 (caso contrario)
*   @param unsigned char permisos : permisos del inodo en caso de que se tenga que crear
*
*   @return 0 (correcto) | -1 (error)
*
*   @errno  EACCES (permisos) 
*           EEXIST (reservar == true y ya existe) 
*           ENOENT (reservar == false y no existe)
*           ENOTDIR (si se intenta crear un fichero en un fichero)
*/
int buscar_entrada(const char *camino_parcial, unsigned int *p_inodo_dir, unsigned int *p_inodo,
    unsigned int *p_entrada, char reservar, unsigned char permisos){

    inodo_t inodo;
    char nom_sig_inodo[60], camino_restante[strlen(camino_parcial)];
    unsigned int num_sig_inodo;
    unsigned char tipo;

    // Comprobamos si se trata del directorio raíz
    if (!strcmp(camino_parcial, "/")) { *p_inodo = 0; *p_entrada = 0; return 0; }

    // Si el directorio no tiene permisos de lectura, error
    inodo = leer_inodo(*p_inodo_dir);
    if (! (inodo.permisos & 4)) { errno = EACCES; return -1; }
    
    // Extraemos el siguiente directorio/fichero del camino
    extraer_camino(camino_parcial, nom_sig_inodo, camino_restante, &tipo);

    // Ya no hay más directorios por recorrer
    if (strlen(camino_restante) <= 1){
        // Si tenemos que reservar y no hay permisos de escritura, error
        if (reservar && (!(inodo.permisos & 2))) { errno = EACCES; return -1; }

        // Comprobar si el inodo padre es un directorio en caso de que haya que resevar
        if (reservar) {
            mi_wait_sem();
            inodo = leer_inodo(*p_inodo_dir);
            if (inodo.tipo != 'd') { errno = ENOTDIR; return -1; }
        }


        *p_entrada = traducir_entrada(*p_inodo_dir, nom_sig_inodo, &num_sig_inodo, reservar, 
            tipo, permisos);

        // Si tenemos que reservar y el inodo ya existe, error
        if (errno == EEXIST) { mi_signal_sem(); return -1; }
        // Si no tenemos que reservar y el inodo no existe
        if (errno == ENOENT) { return -1; }

        if (reservar) {
            inodo = leer_inodo(*p_inodo_dir);
            inodo.ctime = time(NULL);
            inodo.mtime = time(NULL);
            escribir_inodo(inodo, *p_inodo_dir);
            mi_signal_sem();
        }

        *p_inodo = num_sig_inodo; // Número de inodo del fichero/directorio
        return 0;

    } else { // Tenemos que bajar un nivel más

        traducir_entrada(*p_inodo_dir, nom_sig_inodo, &num_sig_inodo, 0, 0, 0);

        // Si no existe el directorio intermedio, error
        if (errno == ENOENT) return -1;

        *p_inodo_dir = num_sig_inodo;
        return buscar_entrada(camino_restante, p_inodo_dir, p_inodo, p_entrada, reservar, permisos);
    }
}

/**
*   Función interna que traduce el nombre de un inodo 'nombre_entrada' en su número de inodo
*   'num_sig_inodo' y su número de entrada '@return' a partir del directorio padre 'p_inodo_dir'
*
*   @param unsigned int *p_inodo_dir : número inodo del directorio padre
*   @param char *nombre_entrada : nombre del inodo a buscar
*   @param unsigned int *num_sig_inodo : contendrá el número de inodo del inodo buscado
*
*   @return 0 (correcto) | -1 (error)
*
*   @errno  EEXIST (reservar == true y ya existe) 
*           ENOENT (reservar == false y no existe)
*/
int traducir_entrada(unsigned int num_dir_padre, char *nombre_entrada, unsigned int *num_sig_inodo, 
    char reservar, char tipo, unsigned char permisos){

    int i, encontrado = 0, nEnt = (BLOCKSIZE)/sizeof(struct _entrada);
    struct _entrada ent, ents[nEnt];
    unsigned int offset, leidos;

    // Recorrido de entradas sobre el inodo padre hasta encontrar la entrada o leerlas todas.
    offset = 0;
    while (((leidos = mi_read_f(num_dir_padre, ents, offset, sizeof(ents))) > 0) && !encontrado) {
        for (i = 0; i < leidos/sizeof(struct _entrada); i++) {
            if (!strcmp(ents[i].nombre, nombre_entrada)) {
                ent = ents[i];
                encontrado = 1;
                break;
            }
            offset += sizeof(struct _entrada);
        }
    }

    // Comprobamos los diferentes casos
    if (reservar) {
        if (encontrado) {
            // Si se debe reservar pero ya existe, error
            errno = EEXIST; return -1;
        } else {
            // Si no existe se reserva
            ent.inodo = reservar_inodo (tipo, permisos);
            strcpy(ent.nombre, nombre_entrada);
            mi_write_f(num_dir_padre, &ent, offset, sizeof(struct _entrada));
        }
    } else {
        if (!encontrado) {
            // Si no se debe reservar y no hemos encontrado la entrada, error
            errno = ENOENT; return -1;
        }
    }

    // Fijamos el output
    *num_sig_inodo = ent.inodo;
    return offset / sizeof(struct _entrada);
}

/**
 *  Función que crea un directorio/fichero en el camino dado con los permisos dados.
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes en directorio padre) 
 *           EEXIST (ya existe) 
 *           ENOTDIR (intenta crear un directorio/fichero dentro de un fichero)
 */
int mi_creat(const char *camino, unsigned char permisos){
    unsigned int p_inodo_dir = 0, p_inodo = 0, p_entrada = 0;
    int out;

    mi_wait_sem();

    out = buscar_entrada(camino, &p_inodo_dir, &p_inodo, &p_entrada, 1, permisos);

    mi_signal_sem();

    return out;
}

/**
 *  Función que lista las entradas del directorio apuntado por el camino dado en el buffer dado
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOTDIR (no es un directorio) 
 *           ENOENT (no existe el directorio)
 */
int mi_dir(const char *camino, char *buffer) {
    unsigned int p_inodo_dir = 0, ninodo = 0, p_entrada = 0;
    inodo_t inodo, inodo_entrada;
    struct _entrada buffer_entradas[BLOCKSIZE/sizeof(struct _entrada)];
    unsigned int i, offset, leido, n_entradas_iter, n_entradas_totales;
    char out_buffer[100];
    struct tm *tm;

    // Buscamos el número de inodo correspondiente al directorio
    if (buscar_entrada(camino, &p_inodo_dir, &ninodo, &p_entrada, 0, 0) < 0) return -1;

    // Leemos el inodo y comprobamos que sea un directorio con permisos de lectura
    inodo = leer_inodo(ninodo);
    if (inodo.tipo != 'd') { errno = ENOTDIR; return -1; }
    if (! (inodo.permisos & 4)) { errno = EACCES; return -1; }

    // Iteramos sobre cada entrada (usando bloques)
    n_entradas_totales = 0;
    leido = 0;
    offset = 0;
    while ((leido = mi_read_f(ninodo, buffer_entradas, offset, BLOCKSIZE)) > 0) {
        offset += leido; // Actualizamos el offset para la siguiente iteración

        n_entradas_iter = leido / sizeof(struct _entrada);
        for (i = 0; i < n_entradas_iter; i++) {
            // Leemos el inodo y llenamos el buffer con su información
            inodo_entrada = leer_inodo(buffer_entradas[i].inodo);
            
            // Tipo y permisos
            strcat(buffer, (inodo_entrada.tipo == 'd')  ? "d" : "-");
            strcat(buffer, (inodo_entrada.permisos & 4) ? "r" : "-");
            strcat(buffer, (inodo_entrada.permisos & 2) ? "w" : "-");
            strcat(buffer, (inodo_entrada.permisos & 1) ? "x" : "-");
            
            // Tamaño
            sprintf(out_buffer, " %10d ", inodo_entrada.tamEnBytesLog);
            strncat(buffer, out_buffer, 12);

            // Tiempo
            tm = localtime(&inodo.mtime);
            sprintf(out_buffer,"%d-%02d-%02d %02d:%02d:%02d ", tm->tm_year+1900, tm->tm_mon+1,
                tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
            strcat(buffer, out_buffer);

            // Nombre del inodo
            strcat(buffer, buffer_entradas[i].nombre);
            strcat(buffer, "\n");
        }
        n_entradas_totales += n_entradas_iter;
    }
    return (leido) ? -1 : n_entradas_totales;
}

/**
 *  Función que crea un fichero en la ruta camino2 que apunta al mismo inodo apuntado por la
 *      ruta camino1.
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           EISDIR (es un directorio) 
 *           ENOENT (no existe el fichero camino1)
 */
int mi_link(const char *camino1, const char *camino2) {
    unsigned int p_inodo_dir, p_inodo, p_entrada, ninodo1;
    inodo_t inodo;
    struct _entrada entrada_dir2;

    mi_wait_sem();

    // Buscamos el número de inodo correspondiente al fichero 1
    p_inodo_dir = 0; p_inodo = 0; p_entrada = 0;
    if (buscar_entrada(camino1, &p_inodo_dir, &p_inodo, &p_entrada, 0, 0) < 0) return -1;

    // Leemos el inodo y comprobamos que sea un fichero con permisos de lectura
    ninodo1 = p_inodo;
    inodo = leer_inodo(ninodo1);
    if (inodo.tipo != 'f') { errno = EISDIR; return -1; }
    if (! (inodo.permisos & 4)) { errno = EACCES; return -1; }

    // Buscamos el número de inodo correspondiente al fichero 2. Si no existe, se crea.
    p_inodo_dir = 0; p_inodo = 0; p_entrada = 0;
    if (buscar_entrada(camino2, &p_inodo_dir, &p_inodo, &p_entrada, 1, 7) < 0
            && errno != EEXIST) return -1;

    // Liberamos el inodo que corresponde al fichero 2. Buscamos la entrada dentro de su
    //      directorio padre y le asignamos el ninodo del fichero 1.
    liberar_inodo(p_inodo);
    if (mi_read_f(p_inodo_dir, &entrada_dir2, sizeof(struct _entrada)*p_entrada, sizeof(struct _entrada)) < 0){
        return -1;
    }

    entrada_dir2.inodo = ninodo1;
    if (mi_write_f(p_inodo_dir, &entrada_dir2, sizeof(struct _entrada)*p_entrada, sizeof(struct _entrada)) < 0){
        return -1;
    }
    
    // Actualizamos el nlinks y ctime del inodo linkeado
    inodo = leer_inodo(ninodo1);
    inodo.nlinks += 1;
    inodo.ctime = time(NULL);
    escribir_inodo(inodo, ninodo1);

    mi_signal_sem();

    return 0;
}

/**
 *  Función que destruye el fichero apuntado por la ruta dada decrementando el atributo nlink
 *      del inodo en 1. Si es 0, se elimina el inodo también.
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOTEMPTY (es un directorio y no está vacío) 
 *           ENOENT (no existe el fichero camino)
 */
int mi_unlink(const char *camino) {
    unsigned int p_inodo_dir, p_inodo, p_entrada;
    inodo_t inodo;
    struct _entrada entrada;

    mi_wait_sem();

    // Buscamos el inodo al que hace referencia el camino
    p_inodo_dir = 0; p_inodo = 0; p_entrada = 0;
    if (buscar_entrada(camino, &p_inodo_dir, &p_inodo, &p_entrada, 0, 0) < 0) return -1;

    // Comprobamos que si es un directorio, esté vacío
    inodo = leer_inodo(p_inodo);
    if (inodo.tipo == 'd' && inodo.tamEnBytesLog > 0) { errno = ENOTEMPTY; return -1; }

    // Decrementamos el nlinks en uno, y si queda en 0. Liberamos el inodo.
    if (! (inodo.nlinks -= 1)){
        liberar_inodo(p_inodo);
    } else {
        inodo.ctime = time(NULL);
        escribir_inodo(inodo, p_inodo);
    }

    // Leemos el directorio que contiene/contenía el inodo a unlinkear y eliminamos la entrada.
    inodo = leer_inodo(p_inodo_dir);
    if (inodo.tamEnBytesLog != sizeof(struct _entrada)*(p_entrada+1)) {
        if (mi_read_f(p_inodo_dir, &entrada, inodo.tamEnBytesLog - sizeof(struct _entrada), 
                    sizeof(struct _entrada)) < 0) {
            return -1;
        }

        if (mi_write_f(p_inodo_dir, &entrada, p_entrada*sizeof(struct _entrada), 
                    sizeof(struct _entrada)) < 0) {
            return -1;
        }
    }
    if (mi_truncar_f(p_inodo_dir, inodo.tamEnBytesLog - sizeof(struct _entrada)) < 0) {
        return -1;
    }

    mi_signal_sem();

    return 0;
}

/**
 *  Función que cambia los permisos del fichero apuntado por la ruta dada 
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOENT (no existe el fichero/directorio)
 */
int mi_chmod(const char *camino, unsigned char permisos) {
    unsigned int p_inodo_dir, p_inodo, p_entrada;

    // Buscamos el inodo al que hace referencia el camino
    p_inodo_dir = 0; p_inodo = 0; p_entrada = 0;
    if (buscar_entrada(camino, &p_inodo_dir, &p_inodo, &p_entrada, 0, 0) < 0) return -1;

    // Llamamos a mi_chmod_f
    mi_chmod_f(p_inodo, permisos);
    
    return 0;
}

/**
 *  Función que devuelve la información relativa al inodo apuntado por la ruta dada
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOENT (no existe el fichero/directorio)
 */
int mi_stat(const char *camino, struct _stat *p_stat) {
    unsigned int p_inodo_dir, p_inodo, p_entrada;

    // Buscamos el inodo al que hace referencia el camino
    p_inodo_dir = 0; p_inodo = 0; p_entrada = 0;
    if (buscar_entrada(camino, &p_inodo_dir, &p_inodo, &p_entrada, 0, 0) < 0) return -1;

    // Llamamos a mi_stat_f
    mi_stat_f(p_inodo, p_stat);

    return 0;
}

/**
 *  Función que lee los datos contenidos en el fichero apuntado por la ruta 'camino'
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOENT (no existe el fichero/directorio)
 *           EOVERFLOW (offset demasiado grande)
 */
int mi_read(const char *camino, void *buf, unsigned int offset, unsigned int nbytes) {
    unsigned int p_inodo_dir, p_entrada;

    // Actualizamos la caché si es necesario
    if (strcmp(camino, gUltimaEntradaLectura.camino)){
        // Camino
        strcpy(gUltimaEntradaLectura.camino, camino);
        // p_inodo
        p_inodo_dir = 0; p_entrada = 0; gUltimaEntradaLectura.p_inodo = 0;
        if (buscar_entrada(camino, &p_inodo_dir, &gUltimaEntradaLectura.p_inodo, 
                    &p_entrada, 0, 0) < 0) {
            return -1;
        }
    }

    return mi_read_f(gUltimaEntradaLectura.p_inodo, buf, offset, nbytes);
}

/**
 *  Función que escribe los datos contenidos en un buffer 'buf' en el fichero apuntado 
 *      por la ruta 'camino'
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOENT (no existe el fichero/directorio)
 *           EOVERFLOW (offset demasiado grande)
 */
int mi_write(const char *camino, const void *buf, unsigned int offset, unsigned int nbytes) {
    unsigned int p_inodo_dir, p_entrada;
    int out;

    mi_wait_sem(); // Entrada SC

    // Actualizamos la caché si es necesario
    if (strcmp(camino, gUltimaEntradaEscritura.camino)){
        // Camino
        strcpy(gUltimaEntradaEscritura.camino, camino);
        // p_inodo
        p_inodo_dir = 0; p_entrada = 0; gUltimaEntradaEscritura.p_inodo = 0;
        if (buscar_entrada(camino, &p_inodo_dir, &gUltimaEntradaEscritura.p_inodo, 
                    &p_entrada, 0, 0) < 0) {
            return -1;
        }
    } 

    out = mi_write_f(gUltimaEntradaEscritura.p_inodo, buf, offset, nbytes);

    mi_signal_sem();

    return out;
}
