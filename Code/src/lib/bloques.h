#include <unistd.h>     // close(), lseek(), write(), read()
#include <sys/types.h>  // open(), lseek()
#include <sys/stat.h>   // open()
#include <fcntl.h>      // open()
#include <errno.h>      // errno
#include <stdio.h>      // fprintf()
#include <stdlib.h>     // exit()

#include "semaforo_mutex_posix.h"

#define BLOCKSIZE 1024  // Tamaño de un bloque en bytes

void mi_wait_sem();
void mi_signal_sem();

/** 
*	Monta el sistema de ficheros. Internamente, abre el fichero que contiene el SF y guarda
*   su descriptor. Si no existe, se crea dicho fichero.
*
*	@param const char *camino : Ruta del fichero
*	@return int descriptor : Descriptor de fichero
*/
int bmount(const char *camino);

/** 
*	Desmonta el sistema de ficheros. Internamente, cierra el descriptor que apunta al fichero
*   que contiene el sistema de ficheros abierto.
*
*	@return 0
*/
int bumount();

/**
*   Función que guarda el contenido de un buffer <buf> en el bloque número <nbloque> del 
*   sistema de ficheros. 
*
*   @param unsigned int nbloque
*   @param const void *buf
*   @return int : número de bytes escritos
*/
int bwrite(unsigned int nbloque, const void *buf);

/**
*   Función que lee y guarda en el buffer <buf> el contenido del bloque de memoria número 
*   <nbloque> del sistema de ficheros. 
*
*   @param unsigned int nbloque
*   @param const void *buf
*   @return int : número de bytes leídos
*/
int bread(unsigned int nbloque, void *buf);
