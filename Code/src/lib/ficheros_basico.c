#include "ficheros_basico.h"

//////////////////////////// DECLARACIÓN DE FUNCIONES PRIVADAS ///////////////////////////////

/**
*   Función que localiza el bit correspondiente al nbloque dentro del mapa de bits  
*
*   @param unsigned int nbloque : número de bloque a localizar dentro del mapa de bits
*   @param unsigned int *bloque : puntero en cuyo contenido se almacenará el número de bloque 
*       correspondiente al 'nbloque' dentro del mapa de bits
*   @param unsigned int *posByte : idem para el byte dentro del bloque del mapa de bits
*   @param unsigned int *posBit : idem para el bit dentro del byte
*/
void localizar_bit(unsigned int nbloque, unsigned int *bloque, unsigned int *posByte, unsigned int *posBit);

/**
*   Inicializa la información de un inodo a default según el tipo y los permisos pasados por
*   parámetro 
*
*   @param inodo_t *inodo : estructura inodo_t cuyo contenido va a ser inicializado
*   @param unsigned char tipo
*   @param unsigned char permisos
*/
void inicializarInodo(inodo_t *inodo, unsigned char tipo, unsigned char permisos);

/**
*   Función que a partir de un número de inodo devuelve el bloque del SF donde se encuentra 
*       dicho inodo
*
*   @param unsigned int ninodo : número de inodo a localizar
*   @return int nbloque : número de bloque del AI que contiene el inodo_t correspondiente
*       al número de inodo 'ninodo'
*/
int localizar_bloque_AI(unsigned int ninodo);

/**
*   Función privada que calcula en base a un número de bloque lógico 'blogico', la cantidad de 
*   niveles por los que tenemos que pasar hasta llegar al bloque físico.
*   
*   @param unsigned int blogico
*   @return unsigned int : número de niveles
*/
unsigned int calcular_niveles(unsigned int blogico);

/**
*   Función privada que calcula el indice dentro de un cierto nivel 'nivel_actual' para pasar al 
*   siguiente nivel en base al numero total de niveles 'num_niveles_totales' a recorrer y el número
*   de bloque lógico al que queremos acceder 'blogico' 
*
*   @param unsigned int blogico
*   @param unsigned int num_niveles_totales
*   @param unsigned int nivel_actual
*
*   @return unsigned int : índice del puntero que lleva hacia el siguiente nivel
*/
unsigned int calcular_siguiente_indice(unsigned int blogico, unsigned int num_niveles_totales, 
    unsigned int nivel_actual);

/** Función privada que libera el bloque fisico apuntado por '*puntero_bfisico', pone a 0 dicho
*   puntero e incrementa el contenido de 'num_bloques_liberados' en 1 
*
*   @param unsigned int *puntero_bfisico
*   @param unsigned int *num_bloques_liberados
*/
void liberar_bloque_desde_inodo (unsigned int *puntero_bfisico, unsigned int *num_bloques_liberados);


//////////////////////////////////////// IMPLEMENTACIÓN ////////////////////////////////////////

/**
*   Función que devuelve el tamaño, en bloques, necesario para mapa de bits teniendo un número 
*   'nbloques' de bloques 
*
*   @param unsigned int nbloques : número de bloques totales
*   @return int tamaño : número de bloques necesarios del mapa de bits para contener 'nbloques'
*/
int tamMB(unsigned int nbloques){
    return nbloques/(8*BLOCKSIZE) + ((nbloques%(8*BLOCKSIZE) == 0) ? 0 : 1);
}

/**
*   Función que devuelve el tamaño, en bloques, necesario para el array de inodos teniendo
*   un número de inodos 'ninodos'
*
*   @param unsigned int ninodos : número de inodos totales
*   @return int tamaño : tamaño en bloques del array de inodos para contener 'ninodos'
*/
int tamAI(unsigned int ninodos){
    return (ninodos*T_INODO)/BLOCKSIZE + (((ninodos*T_INODO)%BLOCKSIZE == 0) ? 0 : 1);
}
    
/**
*   Función que inicializa el superbloque del sistema de ficheros (SF) con los parámetros por
*   defecto en función de la cantidad de bloques 'nbloques' y el número de inodos 'ninodos' 
*
*   @param unsigned int nbloques : número de bloques totales del SF
*   @param unsigned int ninodos : número de inodos totales del SF
*   @return int : 0 (correcto) 
*/
int initSB(unsigned int nbloques, unsigned int ninodos){
    // Declaramos el superbloque (SB)
    superbloque_t sb;

    // Inicializamos todos los datos del SB
    sb.posPrimerBloqueMB = POS_SB + 1;
    sb.posUltimoBloqueMB = sb.posPrimerBloqueMB + tamMB(nbloques) - 1;
    sb.posPrimerBloqueAI = sb.posUltimoBloqueMB + 1;
    sb.posUltimoBloqueAI = sb.posPrimerBloqueAI + tamAI(ninodos) - 1;
    sb.posPrimerBloqueDatos = sb.posUltimoBloqueAI + 1;
    sb.posUltimoBloqueDatos = nbloques - 1;
    sb.posInodoRaiz = 0;
    sb.posPrimerInodoLibre = 0;
    sb.cantBloquesLibres = nbloques - (1 + tamMB(nbloques) + tamAI(ninodos));
    sb.cantInodosLibres = ninodos;
    sb.totBloques = nbloques;
    sb.totInodos = ninodos;

    // Actualizamos el FS con la información del SB
    return bwrite(POS_SB, &sb);
}

/**
*   Función que inicializa el mapa de bits (MB) de acuerdo a la información que se encuentra en el
*   superbloque del SF
*
*   @return int : 0 (correcto) 
*/
int initMB(){
    unsigned char initBloque[BLOCKSIZE];
    int i; // Para iterar por cada uno de los bloques
    superbloque_t sb; // Contendrá información del superbloque

    // Leemos el superbloque
    bread(POS_SB,&sb);

    // Inicializamos el buffer de tamaño de bloque a 0
    memset(initBloque, 0, BLOCKSIZE);

    // Volcamos el contenido del buffer sobre cada uno de los bloques del MB
    for( i=sb.posPrimerBloqueMB ; i<=sb.posUltimoBloqueMB ; i++ ){
        bwrite(i, initBloque);
    }

    // Inicializamos los bloques ocupados por el SB, MB y AI a 1.
    for (i = POS_SB; i <= sb.posUltimoBloqueAI; i++){
        escribir_bit (i, 1);
    }

    return 0;
}

/**
*   Función que inicializa el array de inodos. Los marca como inodos de tipo libre y actualizamos
*   el primer puntero directo con un puntero al siguiente inodo. El último inodo tiene un puntero
*   con un valor UINT_MAX. 
*
*   @return int : 0 (correcto)
*/
int initAI(){
    unsigned int indiceBloqueInodos, i; // Variables de iteración
    inodo_t bloqueInodos[NINODOS_BLOQUE]; // Buffer que inicializará un bloque de inodos
    superbloque_t sb; // Contendrá información del superbloque

    // Leemos el superbloque
    bread(POS_SB,&sb);

    // Se prepara el buffer del primer bloque del AI
    for( i=0 ; i<NINODOS_BLOQUE ; i++){
        bloqueInodos[i].tipo = 'l';
        bloqueInodos[i].punterosDirectos[0] = i + 1;
    }
    
    // Se inicializan todos los bloques AI del SF. En el último, solo se inicializa el buffer.
    for ( indiceBloqueInodos = sb.posPrimerBloqueAI ; \
        indiceBloqueInodos < sb.posUltimoBloqueAI ; indiceBloqueInodos++ ){

        bwrite(indiceBloqueInodos, bloqueInodos);

        for ( i=0 ; i<NINODOS_BLOQUE ; i++ ){
            bloqueInodos[i].punterosDirectos[0] += NINODOS_BLOQUE;
        }
    }

    // Se modifica el último inodo del buffer para que apunte a UINT_MAX y se escribe.
    bloqueInodos[(sb.totInodos-1)%(NINODOS_BLOQUE)].punterosDirectos[0] = UINT_MAX;
    bwrite(indiceBloqueInodos, bloqueInodos);

    return 0;
}

/**
*   Función que escribe el bit del MB correspondiente al nbloque según el valor del parámetro 'bit' 
*
*   @param unsigned int nbloque : número de bloque físico
*   @param unsigned int bit : bit que se escribirá en la posición correspondiente al número de
*       bloque físico 'nbloque' dentro del mapa de bits
*
*   @return int : nbloque (número de bloque correspondiente al bit modificado)
*/
int escribir_bit(unsigned int nbloque, unsigned int bit){
    unsigned int bloque, posByte, posBit;  // Variables que definen la posición del bit a modificar
    unsigned char mascara; // Máscara para modificar el bit
    unsigned char buffer[BLOCKSIZE]; // Buffer del bloque del mapa de bits

    // Obtenemos la localización del bit dentro del mapa de bits
    localizar_bit(nbloque, &bloque, &posByte, &posBit);

    bread(bloque, buffer); // Leemos el bloque

    // Modificamos el bit del bloque en el buffer leído
    mascara = 128;
    mascara >>= posBit;
    if (bit) {
        buffer[posByte] |= mascara;
    } else {
        buffer[posByte] &= ~mascara;
    }

    // Volcamos el buffer a memoria
    bwrite(bloque, buffer);

    return nbloque;
}

/**
*   Función que lee el bit correspondiente al nbloque dentro del MB  
*
*   @param unsigned int nbloque : número de bloque físico
*   @return unsigned char : 1 (bloque ocupado) | 0 (bloque libre) 
*/
unsigned char leer_bit(unsigned int nbloque){
    unsigned int bloque, posBit, posByte; // Variables que definen la posición del bit a modificar
    unsigned char mascara; // Máscara para modificar el bit
    unsigned char buffer[BLOCKSIZE]; // Buffer del bloque del mapa de bits

    // Obtenemos la localización del bit dentro del mapa de bits
    localizar_bit(nbloque, &bloque, &posByte, &posBit);

    // Leemos el bloque
    bread(bloque, buffer); 

    // Creamos la máscara
    mascara = 128;
    mascara >>= posBit;
    mascara &= buffer[posByte];
    mascara >>= 7-posBit;

    return mascara;
}

/**
*   Función que localiza el bit correspondiente al nbloque dentro del mapa de bits  
*
*   @param unsigned int nbloque : número de bloque a localizar dentro del mapa de bits
*   @param unsigned int *bloque : puntero en cuyo contenido se almacenará el número de bloque 
*       correspondiente al 'nbloque' dentro del mapa de bits
*   @param unsigned int *posByte : idem para el byte dentro del bloque del mapa de bits
*   @param unsigned int *posBit : idem para el bit dentro del byte
*/
void localizar_bit(unsigned int nbloque, unsigned int *bloque, unsigned int *posByte, unsigned int *posBit){
    superbloque_t sb; // Contendrá información del superbloque

    // Leemos el superbloque
    bread (POS_SB, &sb);

    // Posición del bloque de mapa de bits donde se encuentra el bit a modificar
    *bloque = (nbloque / (8 * BLOCKSIZE)) + sb.posPrimerBloqueMB; 

    *posByte = (nbloque / 8) % BLOCKSIZE; // Posición del byte dentro del bloque
    *posBit = nbloque % 8; // Posición del bit dentro del byte
}

/**
*   Función que reserva el primer bloque libre y devuelve el número de bloque  
*
*   @return int nbloque : nbloque (número de bloque físico liberado) | -1 (error)
*
*   @errno ENOSPC : no quedan bloques libres
*/
int reservar_bloque(){
    unsigned int posBloque, posByte, posBit, nbloque;
    unsigned char mascara;
    superbloque_t sb; // Contendrá información del superbloque
    unsigned char bufferAux[BLOCKSIZE], bufferBloque[BLOCKSIZE];

    // Leemos el superbloque
    bread(POS_SB,&sb);
    
    // Si no quedan bloques libres, devolvemos -1
    if (!sb.cantBloquesLibres) { errno = ENOSPC; return -1; }

    // Si quedan, se localiza el primer bloque con un bit a 0 del mapa de bits
    memset(bufferAux, 255, BLOCKSIZE);
    posBloque = sb.posPrimerBloqueMB;

    bread(posBloque, bufferBloque);

    while (!memcmp(bufferBloque, bufferAux, BLOCKSIZE)){
        posBloque++;
        bread(posBloque, bufferBloque);
    }

    // Ahora localizamos el byte dentro del bloque
    posByte = 0;
    while (bufferBloque[posByte] == 255){
        posByte++;
    }

    // Por último buscamos el bit 0 dentro del byte
    mascara = 128;
    posBit = 0;
    while (mascara & bufferBloque[posByte]){
        posBit++;
        mascara >>= 1;
    }
 
    // Calculamos la posición del bloque dentro del SF
    nbloque = ((posBloque-sb.posPrimerBloqueMB)*BLOCKSIZE + posByte)*8 + posBit; 

    // Escribimos a 1 el bit que corresponde
    escribir_bit(nbloque, 1);

    // Actualizamos el contador de bloques libres en el SB
    sb.cantBloquesLibres -= 1;
    bwrite(POS_SB, &sb);

    // Inicializamos el bloque a 0
    memset(bufferAux, 0, BLOCKSIZE);
    bwrite(nbloque, bufferAux);

    // Retornamos la posición del bloque reservado
    return nbloque;
}

/**
*   Función que libera un bloque determinado  
*
*   @param unsigned int nbloque : número de bloque físico a liberar
*   @return int : nbloque (número de bloque físico liberado) 
*/
int liberar_bloque(unsigned int nbloque){
    superbloque_t SB;

    // Liberamos el bloque en el MB
    escribir_bit(nbloque, 0);

    // Actualizamos la cantidad de bloques libres en el SB
    bread(POS_SB, &SB);
    SB.cantBloquesLibres += 1;
    bwrite(POS_SB, &SB);

    return nbloque;
}

/**
*   Función que escribe el inodo en la posición ninodo del AI
*
*   @param inodo_t inodo : estructura con la información del inodo
*   @param unsigned int ninodo : número de inodo que se desea sobreescribir
*   @return int : ninodo (número de inodo del AI sobreescrito)
*/
int escribir_inodo(inodo_t inodo, unsigned int ninodo){
    unsigned int posBloqueAI;
    inodo_t buffer[NINODOS_BLOQUE];

    // Leemos el bloque donde se encuentra el inodo
    posBloqueAI = localizar_bloque_AI(ninodo);
    bread(posBloqueAI, buffer); 

    // Modificamos la información correspondiente al inodo
    buffer[ninodo%(NINODOS_BLOQUE)] = inodo;

    // Lo volcamos de nuevo al bloque
    bwrite(posBloqueAI, buffer);

    return ninodo;
}

/**
*   Función que devuelve un inodo de acuerdo al especificado por parámetro  
*
*   @param unsigned int ninodo : número de inodo del AI a leer
*   @return inodo_t : struct del ninodo
*/
inodo_t leer_inodo(unsigned int ninodo){
    inodo_t buffer[NINODOS_BLOQUE];
    int nbloque;

    nbloque = localizar_bloque_AI(ninodo); 
    bread(nbloque, buffer); 

    return buffer[ninodo%(NINODOS_BLOQUE)];
}

/**
*   Función que a partir de un número de inodo devuelve el bloque del SF donde se encuentra 
*       dicho inodo
*
*   @param unsigned int ninodo : número de inodo a localizar
*   @return int : nbloque (número de bloque del AI que contiene el inodo_t correspondiente
*       al número de inodo 'ninodo')
*/
int localizar_bloque_AI(unsigned int ninodo){
    superbloque_t sb; 
    bread(POS_SB,&sb);
    return sb.posPrimerBloqueAI + ninodo/NINODOS_BLOQUE;
}

/**
*   Función que reserva el primer inodo libre, lo inicializa y devuelve su índice  
*
*   @param unsigned char tipo : 'l' si libre, 'd' si directorio, 'f' para fichero
*   @param unsigned char permisos : 0 o 1 para los bits [rwx]
*
*   @return int : ninodo (número de inodo que se ha reservado) | -1 (error)
*
*   @errno ENOSPC : no quedan inodos libres
*/
int reservar_inodo(unsigned char tipo, unsigned char permisos){
    superbloque_t sb; // Contendrá información del superbloque
    inodo_t inodo;
    unsigned int ninodo;

    // Leemos el superbloque en busca de inodos libres
    bread(POS_SB,&sb);
    if ((ninodo = sb.posPrimerInodoLibre) == UINT_MAX) { errno = ENOSPC; return -1; }

    // Leemos el primer inodo libre
    inodo = leer_inodo(ninodo);

    // Actualizamos el superbloque con la posición del siguiente inodo libre
    sb.posPrimerInodoLibre = inodo.punterosDirectos[0];
    sb.cantInodosLibres -= 1;
    bwrite(POS_SB, &sb);

    // Inicializamos el inodo y lo escribimos en el AI
    inicializarInodo(&inodo, tipo, permisos);
    escribir_inodo(inodo, ninodo);

    // Retornamos su índice en el AI
    return ninodo;
}

/**
*   Inicializa la información de un inodo a default según el tipo y los permisos pasados por
*   parámetro 
*
*   @param inodo_t *inodo : estructura inodo_t cuyo contenido va a ser inicializado
*   @param unsigned char tipo
*   @param unsigned char permisos
*/
void inicializarInodo(inodo_t *inodo, unsigned char tipo, unsigned char permisos){
    int i;
    inodo->tipo = tipo;
    inodo->permisos = permisos;
    inodo->nlinks = 1;
    inodo->tamEnBytesLog = 0;
    inodo->atime = time(NULL);
    inodo->mtime = time(NULL);
    inodo->ctime = time(NULL);
    inodo->numBloquesOcupados = 0;
    for(i = 0; i<MAX_BLOGICO_PDIR; i++){
        inodo->punterosDirectos[i] = 0;
    }
    for(i = 0; i<3; i++){
        inodo->punterosIndirectos[i] = 0;
    }
}

/**
*   Traduce un bloque logico 'blogico' de un inodo 'ninodo' al bloque físico correspondiente. Solo
*    si reservar vale 1, se reservaran nuevos bloques logicos en caso de no estarlos.
*
*   @param unsigned int ninodo : Indice del inodo en el AI
*   @param unsigned int blogico : Indice del bloque lógico dentro del inodo 'ninodo'
*   @param char reservar : 1 para reservar bloques lógicos en caso de no estarlos, 0 en caso contrario.
*
*   @return int bfisico : nbloque_fisico (número de bloque físico correspondiente a 'blogico') |
*       -1 (error) 
*
*   @errno ENOSPC : no quedan bloques libres
*   @errno EOVERFLOW : blogico demasiado grande
*/
int traducir_bloque_inodo(unsigned int ninodo, unsigned int blogico, char reservar){
    
    inodo_t inodo; /* Contendrá el inodo 'ninodo' */ 
    unsigned int *ptr;
    unsigned int bfisico, num_bloques_reservados, nivel, num_niveles, indice;
    unsigned int bufferPunteros[BLOCKSIZE/sizeof(unsigned int)];

    // Devolver error si es un blogico que no podemos direccionar
    if (blogico > MAX_BLOGICO_PDIR + MAX_BLOGICO_PIND1 + MAX_BLOGICO_PIND2 + MAX_BLOGICO_PIND3 - 1){
        errno = EOVERFLOW; return -1; 
    }

    inodo = leer_inodo(ninodo);

    num_bloques_reservados = 0;
    ptr = (num_niveles = calcular_niveles(blogico)) ? 
            inodo.punterosIndirectos + num_niveles - 1  : inodo.punterosDirectos + blogico;

    // Miramos el nivel 0 - Punteros directos o indirectos
    if (!(*ptr)){
        if (!reservar) return 0;

        *ptr = reservar_bloque(); if (*ptr < 0) return -1;
        ++num_bloques_reservados;
    }

    // Miramos los diferentes niveles en caso de tratarse de punteros indirectos
    bfisico = *ptr;
    for ( nivel = 1; nivel <= num_niveles; ++nivel ){
        
        bread (bfisico, bufferPunteros);

        indice = calcular_siguiente_indice (blogico, num_niveles, nivel);

        if (!bufferPunteros[indice]) {
            if (!reservar) return 0;

            bufferPunteros[indice] = reservar_bloque(); if (bufferPunteros[indice] < 0) return -1;
            ++num_bloques_reservados;
            bwrite (bfisico, bufferPunteros);
        }

        bfisico = bufferPunteros[indice];
    }

    // Si hemos tenido que reservar bloques, actualizamos la información del inodo
    if (num_bloques_reservados){
        inodo.numBloquesOcupados += num_bloques_reservados;
        inodo.ctime = time(NULL);
        escribir_inodo (inodo, ninodo);
    }

    return bfisico;
}

/**
*   Libera un inodo 'ninodo' junto a todos los bloques que tiene reservados y actualiza la lista
*       de inodos libres en el superbloque.
*
*   @param unsigned int ninodo : índice del inodo en el AI
*   @return int : ninodo (índice del inodo liberado en el AI) 
*/
int liberar_inodo (unsigned int ninodo){
    superbloque_t sb;
    inodo_t inodo;

    // Liberamos todos los bloques del inodo
    liberar_bloques_inodo(ninodo, 0);

    // Marcamos el inodo como libre 
    inodo = leer_inodo(ninodo);
    inodo.tipo = 'l';

    // Actualizamos la lista y el contador de inodos libres en el superbloque
    bread(POS_SB, &sb);
    inodo.punterosDirectos[0] = sb.posPrimerInodoLibre;
    sb.posPrimerInodoLibre = ninodo;
    sb.cantInodosLibres += 1;

    // Guardamos los cambios
    escribir_inodo(inodo, ninodo);
    bwrite(POS_SB, &sb);

    return ninodo;
}

/**
*   Libera todos los bloques lógicos de un inodo 'ninodo' con un índice igual o superior a 'blogico'.
*
*   @param unsigned int ninodo : índice del inodo en el AI
*   @param unsigned int blogico : índice del primer bloque lógico a liberar
*   @return unsigned int : bloques_liberados (cantidad de bloques lógicos liberados)
*/
int liberar_bloques_inodo (unsigned int ninodo, unsigned int blogico){

    inodo_t inodo;
    int nivel;
    unsigned int bloques_liberados, ultimo_blogico, bloque_reservado, num_niveles, indice, blog;
    unsigned int buffer_puntero_bloques[3][MAX_BLOGICO_PIND1], mascara_punteros[MAX_BLOGICO_PIND1];
    unsigned int *pn[4]; // Array de 4 punteros a unsigned int
    unsigned int *aux;

    bloques_liberados = 0;

    inodo = leer_inodo(ninodo);
    ultimo_blogico = inodo.tamEnBytesLog/BLOCKSIZE;
    memset(mascara_punteros, 0, BLOCKSIZE);

    for (blog = blogico; blog <= ultimo_blogico; ++blog){

        pn[0] = (unsigned int *) (num_niveles = calcular_niveles(blog)) ? 
            (inodo.punterosIndirectos + num_niveles - 1) : (inodo.punterosDirectos + blog);

        bloque_reservado = 1;
        for ( nivel = 0 /* El 0 es el nivel 1 */; nivel < num_niveles; ++nivel ){
            aux = pn[nivel];
            if (*aux){
                bread(*aux, buffer_puntero_bloques[nivel]);

                indice = calcular_siguiente_indice(blog, num_niveles, nivel);
                pn[nivel+1] = (unsigned int *) buffer_puntero_bloques[nivel] + indice;
            } else {
                bloque_reservado = 0;
                break;
            }
        }
        if (! *(pn[num_niveles])) bloque_reservado = 0;

        if (bloque_reservado){
            nivel = num_niveles;
            
            liberar_bloque_desde_inodo (pn[nivel], &bloques_liberados);
            --nivel;

            while ((nivel >= 0) && 
                (! memcmp(mascara_punteros, buffer_puntero_bloques[nivel], MAX_BLOGICO_PIND1))) {
                    liberar_bloque_desde_inodo (pn[nivel], &bloques_liberados);
                    --nivel;
            }

            if (nivel >= 0){
                bwrite(*pn[nivel], buffer_puntero_bloques[nivel]);
            }
        }
    }

    if (bloques_liberados){
        inodo.numBloquesOcupados -= bloques_liberados;
        inodo.ctime = time(NULL);
        escribir_inodo(inodo, ninodo);
    }

	return bloques_liberados;
}
    
/** Función privada que libera el bloque fisico apuntado por '*puntero_bfisico', pone a 0 dicho
*   puntero e incrementa el contenido de 'num_bloques_liberados' en 1 
*
*   @param unsigned int *puntero_bfisico
*   @param unsigned int *num_bloques_liberados
*/
void liberar_bloque_desde_inodo (unsigned int *puntero_bfisico, unsigned int *num_bloques_liberados){
    liberar_bloque(*puntero_bfisico);
    *puntero_bfisico = 0;
    *num_bloques_liberados += 1;
}

/**
*   Función privada que calcula el indice dentro de un cierto nivel 'nivel_actual' para pasar al 
*   siguiente nivel en base al numero total de niveles 'num_niveles_totales' a recorrer y el número
*   de bloque lógico al que queremos acceder 'blogico' 
*
*   @param unsigned int blogico
*   @param unsigned int num_niveles_totales
*   @param unsigned int nivel_actual
*
*   @return unsigned int : índice del puntero que lleva hacia el siguiente nivel
*/
unsigned int calcular_siguiente_indice(unsigned int blogico, unsigned int num_niveles_totales, 
    unsigned int nivel_actual){

    unsigned int resultado;

    switch (num_niveles_totales) {
        // Indirecto de un nivel
        case 1: resultado = blogico - MAX_BLOGICO_PDIR; break;

        // Indirecto de dos niveles
        case 2:
            resultado = blogico - MAX_BLOGICO_PDIR - MAX_BLOGICO_PIND1;
            switch (nivel_actual) {
                case 1: resultado /= (MAX_BLOGICO_PIND1); break;
                case 2: resultado %= (MAX_BLOGICO_PIND1); break;
            }
            break;

        // Indirecto de tres niveles
        case 3:
            resultado = blogico - MAX_BLOGICO_PDIR - MAX_BLOGICO_PIND1 - MAX_BLOGICO_PIND2;
            switch (nivel_actual) {
                case 1: resultado /= (MAX_BLOGICO_PIND2); break;
                case 2: resultado = (resultado % (MAX_BLOGICO_PIND2)) / (MAX_BLOGICO_PIND1); break;
                case 3: resultado = (resultado % (MAX_BLOGICO_PIND2)) % (MAX_BLOGICO_PIND1); break;
            }
            break;

        default:
            resultado = 0; // Este caso nunca se dará
    }

    return resultado;
}

/**
*   Función privada que calcula en base a un número de bloque lógico 'blogico', la cantidad de 
*   niveles por los que tenemos que pasar hasta llegar al bloque físico.
*   
*   @param unsigned int blogico
*   @return unsigned int : número de niveles
*/
unsigned int calcular_niveles(unsigned int blogico){
    if (blogico < MAX_BLOGICO_PDIR) { return 0; }
    else if (blogico < MAX_BLOGICO_PDIR + MAX_BLOGICO_PIND1) { return 1; }
    else if (blogico < MAX_BLOGICO_PDIR + MAX_BLOGICO_PIND1 + MAX_BLOGICO_PIND2) { return 2; }
    else { return 3; }  
}

