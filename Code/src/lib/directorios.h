#include "ficheros.h"

#define LONG_ENTRADA 60

struct _entrada {
    char nombre[LONG_ENTRADA];
    unsigned int inodo;
};

/**
 *  Función que crea un directorio/fichero en el camino dado con los permisos dados.
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes en directorio padre) 
 *           EEXIST (ya existe) 
 */
int mi_creat(const char *camino, unsigned char permisos);

/**
 *  Función que lista las entradas del directorio apuntado por el camino dado en el buffer dado
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOTDIR (no es un directorio) 
 *           ENOENT (no existe el directorio)
 */
int mi_dir(const char *camino, char *buffer);

/**
 *  Función que crea un fichero en la ruta camino2 que apunta al mismo inodo apuntado por la
 *      ruta camino1.
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           EISDIR (es un directorio) 
 *           ENOENT (no existe el fichero camino1)
 */
int mi_link(const char *camino1, const char *camino2);

/**
 *  Función que destruye el fichero apuntado por la ruta dada decrementando el atributo nlink
 *      del inodo en 1. Si es 0, se elimina el inodo también.
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOTEMPTY (es un directorio y no está vacío) 
 *           ENOENT (no existe el fichero camino)
 */
int mi_unlink(const char *camino);

/**
 *  Función que cambia los permisos del fichero apuntado por la ruta dada 
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOENT (no existe el fichero/directorio)
 */
int mi_chmod(const char *camino, unsigned char permisos);

/**
 *  Función que devuelve la información relativa al inodo apuntado por la ruta dada
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOENT (no existe el fichero/directorio)
 */
int mi_stat(const char *camino, struct _stat *p_stat);

/**
 *  Función que lee los datos contenidos en el fichero apuntado por la ruta 'camino'
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOENT (no existe el fichero/directorio)
 *           EOVERFLOW (offset demasiado grande)
 */
int mi_read(const char *camino, void *buf, unsigned int offset, unsigned int nbytes);

/**
 *  Función que escribe los datos contenidos en un buffer 'buf' en el fichero apuntado 
 *      por la ruta 'camino'
 *
 *   @return 0 (correcto) | -1 (error)
 *
 *   @errno  EACCES (permisos insuficientes)
 *           ENOENT (no existe el fichero/directorio)
 *           EOVERFLOW (offset demasiado grande)
 */
int mi_write(const char *camino, const void *buf, unsigned int offset, unsigned int nbytes);
