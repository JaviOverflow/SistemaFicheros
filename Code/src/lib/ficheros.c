#include "ficheros.h"

/**
*	Escribe 'nbytes' del contenido de un buffer de memoria 'buf_original' en el contenido de un 
*   inodo 'inodo' a partir del byte 'offset'
*
*	@param unsigned int ninodo : Indice del inodo en el AI
*	@param const void *buf_original : Buffer de memoria
*	@param unsigned int offset : Posición del primer byte del inodo a escribir
*	@param unsigned int nbytes : Número de bytes que se deben escribir a partir del offset
*
*	@return int : (nbytes) Cantidad de bytes escritos | -1 (error)
*
*   @error EACCES | EOVERFLOW | ENOSPC
*/
int mi_write_f (unsigned int ninodo, const void *buf_original, unsigned int offset, unsigned int nbytes){

	inodo_t inodo;
	unsigned int primerBloqueLogico, ultimoBloqueLogico;
	int bytes, desp1, bloqueFisico, indice;
	unsigned char buf_bloque [BLOCKSIZE];

    mi_wait_sem();

	// Error si permiso de escritura 'w' == 0
	inodo = leer_inodo (ninodo);
	if (! (inodo.permisos & 2)) { errno = EACCES; return -1; }

	bytes = 0;
	primerBloqueLogico = offset/BLOCKSIZE;
	ultimoBloqueLogico = (offset + nbytes-1) / BLOCKSIZE;
	desp1 = offset % BLOCKSIZE;

	// Caso 1 : escritura en un único bloque
	if (primerBloqueLogico == ultimoBloqueLogico){

		//Obtenemos el bloque físico y lo almacenamos en buf_bloque
		bloqueFisico = traducir_bloque_inodo (ninodo, primerBloqueLogico, 1);
        if (bloqueFisico < 0) return -1;
		bread (bloqueFisico, buf_bloque);

        // Sobreescribimos con los datos de buf_original
		memcpy (buf_bloque+desp1, buf_original, nbytes);
		bwrite (bloqueFisico, buf_bloque);

		bytes += nbytes;
		
    // Caso 2 : escritura en dos o más bloques
	} else { 
		
        // 1.- BLOQUE INICIAL
        // Lectura
        bloqueFisico = traducir_bloque_inodo (ninodo, primerBloqueLogico, 1);
        if (bloqueFisico < 0) return -1;
		bread (bloqueFisico, buf_bloque);

        // Tratamiento
		memcpy (buf_bloque+desp1, buf_original, BLOCKSIZE - desp1); 

        // Escritura
		bwrite (bloqueFisico, buf_bloque);
		bytes += BLOCKSIZE - desp1;

		// 2.- BLOQUES INTERMEDIOS
		for (indice = primerBloqueLogico+1; indice < ultimoBloqueLogico; indice++){
			
            // Calculamos el bloque físico y preparamos el buffer con los datos
            bloqueFisico = traducir_bloque_inodo(ninodo,indice,1);
            if (bloqueFisico < 0) return -1;

			memcpy (buf_bloque, buf_original+bytes, BLOCKSIZE);

            // Escritura
            bytes += bwrite (bloqueFisico, buf_bloque);
		}

		// 3.- ÚLTIMO BLOQUE 
        // Lectura
        bloqueFisico = traducir_bloque_inodo(ninodo,ultimoBloqueLogico,1);
        if (bloqueFisico < 0) return -1;
		bread(bloqueFisico, buf_bloque);

        // Tratamiento
		memcpy (buf_bloque, buf_original+bytes, nbytes - bytes);

        // Escritura
		bwrite(bloqueFisico, buf_bloque);

		bytes += nbytes - bytes;

	}

    if (bytes != 0){
        // Volvemos a leer el inodo porque puede que se haya modificado al reservar bloques nuevos
            // en traducir_bloque_inodo
        inodo = leer_inodo (ninodo);
        //Actualizar el tamaño en bytes lógicos
        if (inodo.tamEnBytesLog < offset+nbytes){
            inodo.tamEnBytesLog = offset+nbytes;
        }

        //Actualizar el mtime y el ctime
        inodo.mtime = time (NULL);
        inodo.ctime = time (NULL);

        //Escribimos el inodo modificado	
        escribir_inodo (inodo, ninodo);
    }

    mi_signal_sem(); // Salida SC

	return bytes;

}

/**
*	Vuelca en un buffer 'buf_original' un número de bytes 'nbytes' de información que contiene un 
*   inodo 'ninodo' en la posición 'offset'
*
*	@param unsigned int ninodo : Indice del inodo en el AI
*	@param const void *buf_original : Buffer donde se volcarán los datos
*	@param unsigned int offset : Posición del primer byte del inodo a leer
*	@param unsigned int nbytes : Número de bytes que se deben leer a partir del offset
*
*	@return int bytes: Cantidad de bytes leídos | -1 (Error)
*
*	@errno EACCES | EOVERFLOW
*/
int mi_read_f (unsigned int ninodo, void *buf_original, unsigned int offset, unsigned int nbytes){

	inodo_t inodo;
	int bytes, desp1, bloqueFisico, indice;
	unsigned int primerBloqueLogico, ultimoBloqueLogico;
	unsigned char buf_bloque [BLOCKSIZE];

    mi_wait_sem(); // Entrada SC

	//Si no tenemos permiso de lectura 'r', devolvemos error de permisos
	inodo = leer_inodo (ninodo);
	if (! (inodo.permisos & 4)) { errno = EACCES; mi_signal_sem(); return -1; }
	if (offset >= inodo.tamEnBytesLog) { mi_signal_sem(); return 0; }
	
    //Escribimos el inodo modificado	
    inodo.atime = time (NULL);
    escribir_inodo (inodo, ninodo);

    mi_signal_sem(); // Salida SC


	if ((offset + nbytes) >= inodo.tamEnBytesLog) nbytes = (inodo.tamEnBytesLog - offset); 
    
	
	bytes = 0;
	primerBloqueLogico = offset / BLOCKSIZE;
	ultimoBloqueLogico = (offset + (nbytes - 1)) / BLOCKSIZE;
	desp1 = (offset % BLOCKSIZE);
	memset (buf_bloque, 0, BLOCKSIZE);

	// Caso 1 : primer y último bloque coinciden
	if (primerBloqueLogico == ultimoBloqueLogico){
		
        // Obtenemos el bloque físico, si no existe no tiene sentido continuar
        bloqueFisico = traducir_bloque_inodo (ninodo, primerBloqueLogico, 0); 
        if (bloqueFisico < 0) return -1; // offset y/o nbytes demasiado grandes

        // Si es un bloque vacío de un fichero disperso, seguimos
		if (bloqueFisico){
            // Leemos los datos
            bread (bloqueFisico, buf_bloque);
            // Escribimos el buffer de entrada
            memcpy (buf_original, buf_bloque + desp1, nbytes);
        }
		bytes += nbytes;
		
    // Caso 2 : lectura de dos o más bloques
	} else { 

        // 1.- BLOQUE INICIAL
        bloqueFisico = traducir_bloque_inodo (ninodo, primerBloqueLogico, 0); 
        if (bloqueFisico < 0) return -1; // offset y/o nbytes demasiado grandes

        // Si es un bloque vacío de un fichero disperso, seguimos
		if (bloqueFisico){
            bread (bloqueFisico, buf_bloque);
            memcpy (buf_original, buf_bloque + desp1, BLOCKSIZE - desp1); 
        }
		bytes += BLOCKSIZE - desp1;

		// 2.- BLOQUES INTERMEDIOS 
		for (indice = primerBloqueLogico+1; indice < ultimoBloqueLogico; indice++){
			
            bloqueFisico = traducir_bloque_inodo (ninodo, indice, 0);
            if (bloqueFisico < 0) return -1; // offset y/o nbytes demasiado grandes

            // Si es un bloque vacío de un fichero disperso, seguimos
            if (bloqueFisico){
                bread (bloqueFisico, buf_bloque);
                memcpy (buf_original + bytes, buf_bloque, BLOCKSIZE);
			}
			bytes += BLOCKSIZE;		

		}
		
		// 3.- ÚLTIMO BLOQUE
        bloqueFisico = traducir_bloque_inodo (ninodo, ultimoBloqueLogico, 0);
        if (bloqueFisico < 0) return -1; // offset y/o nbytes demasiado grandes

        // Si es un bloque vacío de un fichero disperso acabamos
		if (bloqueFisico){
            bread (bloqueFisico, buf_bloque);
            memcpy (buf_original + bytes, buf_bloque, nbytes - bytes);
        }

		bytes = nbytes; 
	}

	return bytes;
}

/**
*   Función que cambia los permisos de un inodo   
*
*   @param unsigned int ninodo : número de inodo 
*   @param unsigned char permisos : permisos actualizados
*   @return int : 0 (correcto) 
*/
int mi_chmod_f (unsigned int ninodo, unsigned char permisos){
    inodo_t inodo;

    mi_wait_sem(); // Entrada SC

    // Leemos el inodo, lo actualizamos y lo volvemos a escribir
    inodo = leer_inodo(ninodo);
    inodo.permisos = permisos;
    inodo.ctime = time(NULL);
    escribir_inodo (inodo, ninodo);

    mi_signal_sem(); // Salida SC

    return 0;
}

/**
*   Función que trunca un inodo 'ninodo' a partir de una posición de bytes 'nbytes'
*
*   @param unsigned int ninodo : número de inodo
*   @param unsigned int nbytes : posición del byte a partir del cual se debe truncar
*   @return int : 0 (correcto) | -1 (error)
*
*   @errno EACCES : permiso de escritura;
*/
int mi_truncar_f (unsigned int ninodo, unsigned int nbytes){
    inodo_t inodo;

    // Comprobamos que el inodo tenga permiso de escritura
    inodo = leer_inodo (ninodo);
    if (! (inodo.permisos & 2)) { errno = EACCES; return -1; }

    // Liberamos los bloques a truncar
    liberar_bloques_inodo (ninodo, ((nbytes - 1) / BLOCKSIZE) + 1);

    // Actualizamos la información del inodo y lo escribimos
    inodo = leer_inodo (ninodo);
    inodo.tamEnBytesLog = nbytes;
    inodo.ctime = time(NULL);
    inodo.mtime = time(NULL);
    escribir_inodo (inodo, ninodo);

    return 0;
}

/**
*   Función que deposita en el contenido de 'p_stat' la metainformación del fichero/directorio
*       con índice 'ninodo'
*
*   @param unsigned int ninodo : número de inodo
*   @param struct _stat *p_stat : estructura donde se escribirá la metainformación
*   @return int : 0 (correcto)
*/
int mi_stat_f (unsigned int ninodo, struct _stat *p_stat){
    inodo_t inodo;

    // Leemos el inodo
    inodo = leer_inodo (ninodo);

    // Actualizamos el contenido del puntero stat pasado por parámetro
    p_stat->tipo = inodo.tipo;
    p_stat->permisos = inodo.permisos;
    p_stat->atime = inodo.atime;
    p_stat->mtime = inodo.mtime;
    p_stat->ctime = inodo.ctime;
    p_stat->nlinks = inodo.nlinks;
    p_stat->tamEnBytesLog = inodo.tamEnBytesLog;
    p_stat->numBloquesOcupados = inodo.numBloquesOcupados;

    return 0;
}
