#include <time.h>
#include <limits.h>
#include <string.h>

#include "bloques.h"

// Constantes generales del sistema de ficheros
#define POS_SB   0   // El superbloque empieza en el primer bloque del FS
#define T_INODO 128 // Tamaño de un INODO
#define NINODOS_BLOQUE (BLOCKSIZE/T_INODO) // Número de inodos por bloque en el AI

// Constantes de inodo
/* Como el preprocesador no tiene acceso a la función sizeof(int), calculamos el sizeof int
    mediante desplazamientos al máximo*/
#if INT_MAX >> 16 == 0
    #define SIZEOF_INT 2
#else
    #define SIZEOF_INT 4
#endif

#define MAX_BLOGICO_PDIR    12
#define MAX_BLOGICO_PIND1   BLOCKSIZE / SIZEOF_INT
#define MAX_BLOGICO_PIND2   MAX_BLOGICO_PIND1 * MAX_BLOGICO_PIND1
#define MAX_BLOGICO_PIND3   MAX_BLOGICO_PIND1 * MAX_BLOGICO_PIND1 * MAX_BLOGICO_PIND1
#define MAX_TAM_INODO       ((unsigned long)(MAX_BLOGICO_PDIR + MAX_BLOGICO_PIND1 + MAX_BLOGICO_PIND2\
                            + MAX_BLOGICO_PIND3) * BLOCKSIZE)

/* Estructura que define el superbloque  */
typedef union _superbloque {
    struct{
        unsigned int posPrimerBloqueMB; // Posición del primer bloque del MB
        unsigned int posUltimoBloqueMB; // Posición del último bloque del MB
        unsigned int posPrimerBloqueAI; // Posición del primer bloque del array de inodos
        unsigned int posUltimoBloqueAI; // Posición del último bloque del array de inodos
        unsigned int posPrimerBloqueDatos;  // Posición del primer bloque de datos
        unsigned int posUltimoBloqueDatos;  // Posición del último bloque de datos
        unsigned int posInodoRaiz;      // Posición del inodo del directorio raíz
        unsigned int posPrimerInodoLibre;   // Posición del primer inodo libre
        unsigned int cantBloquesLibres;     // Cantidad de bloques libres
        unsigned int cantInodosLibres;      // Cantidad de inodos libres
        unsigned int totBloques;    // Cantidad total de blqoues
        unsigned int totInodos;     // Cantidad total de inodos
    };
    char padding[BLOCKSIZE]; // Relleno
} superbloque_t;

/* Estructura que define un inodo  */
typedef union _inodo {
    struct{
        unsigned char tipo; // Tipo (libre, directorio o fichero)
        unsigned char permisos; // Permisos (lectura y/o escritura y/o ejecución)
        time_t atime;   // Fecha y hora del último acceso a datos
        time_t mtime;   // Fecha y hora de la última modificación de datos
        time_t ctime;   // Fecha y hora de la última modificación del inodo
        unsigned int nlinks;    // Cantidad de enlaces de entradas en directorio
        unsigned int tamEnBytesLog; // Tamaño en bytes lógicos
        unsigned int numBloquesOcupados;    // Cantidad de bloques ocupados en la zona de datos
        unsigned int punterosDirectos[MAX_BLOGICO_PDIR];  // 12 punteros a bloques directos
        unsigned int punterosIndirectos[3]; // 3 punteros a bloques indirectos: simple, doble y triple
    };
    char padding[128];// Relleno
} inodo_t;

/**
*   Función que devuelve el tamaño, en bloques, necesario para mapa de bits teniendo un número 
*   'nbloques' de bloques 
*
*   @param unsigned int nbloques : número de bloques totales
*   @return int tamaño : número de bloques necesarios del mapa de bits para contener 'nbloques'
*/
int tamMB(unsigned int nbloques);

/**
*   Función que devuelve el tamaño, en bloques, necesario para el array de inodos teniendo
*   un número de inodos 'ninodos'
*
*   @param unsigned int ninodos : número de inodos totales
*   @return int tamaño : tamaño en bloques del array de inodos para contener 'ninodos'
*/
int tamAI(unsigned int ninodos);

/**
*   Función que inicializa el superbloque del sistema de ficheros (SF) con los parámetros por
*   defecto en función de la cantidad de bloques 'nbloques' y el número de inodos 'ninodos' 
*
*   @param unsigned int nbloques : número de bloques totales del SF
*   @param unsigned int ninodos : número de inodos totales del SF
*   @return int : 0 (correcto) 
*/
int initSB(unsigned int nbloques, unsigned int ninodos);

/**
*   Función que inicializa el mapa de bits (MB) de acuerdo a la información que se encuentra en el
*   superbloque del SF
*
*   @return int : 0 (correcto)
*/
int initMB();

/**
*   Función que inicializa el array de inodos. Los marca como inodos de tipo libre y actualizamos
*   el primer puntero directo con un puntero al siguiente inodo. El último inodo tiene un puntero
*   con un valor UINT_MAX. 
*
*   @return int : 0 (correcto) 
*/
int initAI();

/**
*   Función que escribe el bit del MB correspondiente al nbloque según el valor del parámetro 'bit' 
*
*   @param unsigned int nbloque : número de bloque físico
*   @param unsigned int bit : bit que se escribirá en la posición correspondiente al número de
*       bloque físico 'nbloque' dentro del mapa de bits
*
*   @return int : nbloque (número de bloque correspondiente al bit modificado) 
*/
int escribir_bit(unsigned int nbloque, unsigned int bit);

/**
*   Función que lee el bit correspondiente al nbloque dentro del MB  
*
*   @param unsigned int nbloque : número de bloque físico
*   @return unsigned char : 1 (bloque ocupado) | 0 (bloque libre)
*/
unsigned char leer_bit(unsigned int nbloque);

/**
*   Función que reserva el primer bloque libre y devuelve el número de bloque  
*
*   @return int nbloque : nbloque (número de bloque físico liberado) | -1 (error)
*
*   @errno ENOSPC : no quedan bloques libres
*/
int reservar_bloque();

/**
*   Función que libera un bloque determinado  
*
*   @param unsigned int nbloque : número de bloque físico a liberar
*   @return int : nbloque (número de bloque físico liberado) 
*/
int liberar_bloque(unsigned int nbloque);

/**
*   Función que escribe el inodo en la posición ninodo del AI
*
*   @param inodo_t inodo : estructura con la información del inodo
*   @param unsigned int ninodo : número de inodo que se desea sobreescribir
*   @return int : ninodo (número de inodo del AI sobreescrito) 
*/
int escribir_inodo(inodo_t inodo, unsigned int ninodo);

/**
*   Función que devuelve un inodo de acuerdo al especificado por parámetro  
*
*   @param unsigned int ninodo : número de inodo del AI a leer
*   @return inodo_t : struct del ninodo
*/
inodo_t leer_inodo(unsigned int ninodo);

/**
*   Función que reserva el primer inodo libre, lo inicializa y devuelve su índice  
*
*   @param unsigned char tipo : 'l' si libre, 'd' si directorio, 'f' para fichero
*   @param unsigned char permisos : 0 o 1 para los bits [rwx]
*
*   @return int : ninodo (número de inodo que se ha reservado) | -1 (error)
*
*   @errno ENOSPC : no quedan inodos libres
*/
int reservar_inodo(unsigned char tipo, unsigned char permisos);

/**
*   Traduce un bloque logico 'blogico' de un inodo 'ninodo' al bloque físico correspondiente. Solo
*    si reservar vale 1, se reservaran nuevos bloques logicos en caso de no estarlos.
*
*   @param unsigned int ninodo : Indice del inodo en el AI
*   @param unsigned int blogico : Indice del bloque lógico dentro del inodo 'ninodo'
*   @param char reservar : 1 para reservar bloques lógicos en caso de no estarlos, 0 en caso contrario.
*
*   @return int bfisico : nbloque_fisico (número de bloque físico correspondiente a 'blogico') |
*       -1 (error) 
*
*   @errno ENOSPC : no quedan bloques libres
*   @errno EOVERFLOW : blogico demasiado grande
*/
int traducir_bloque_inodo(unsigned int ninodo, unsigned int blogico, char reservar);
    
/**
*   Libera un inodo 'ninodo' junto a todos los bloques que tiene reservados y actualiza la lista
*       de inodos libres en el superbloque.
*
*   @param unsigned int ninodo : índice del inodo en el AI
*   @return int : ninodo (índice del inodo liberado en el AI) 
*/
int liberar_inodo (unsigned int ninodo);

/**
*   Libera todos los bloques lógicos de un inodo 'ninodo' con un índice igual o superior a 'blogico'.
*
*   @param unsigned int ninodo : índice del inodo en el AI
*   @param unsigned int blogico : índice del primer bloque lógico a liberar
*   @return unsigned int : bloques_liberados (cantidad de bloques lógicos liberados) 
*/
int liberar_bloques_inodo (unsigned int ninodo, unsigned int blogico);
