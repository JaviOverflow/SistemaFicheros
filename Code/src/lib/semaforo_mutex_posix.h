#include <semaphore.h>
#include <stdlib.h>

#define SEM_NAME "/mymutex" /* Usamos este nombre para el semáforo mutex */
#define SEM_INIT_VALUE 1 /* Valor inicial de los mutex */

sem_t *init_sem();
void delete_sem();
void signal_sem(sem_t *sem);
void wait_sem(sem_t *sem);
