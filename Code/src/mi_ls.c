#include "lib/directorios.h"

int main(int argc, char **argv){
    char buffer[4096];

    // Comprobamos el número de argumentos
    if (argc != 3) {
        fprintf(stderr, "Uso: mi_ls DISCO RUTA\n");
        exit(EXIT_FAILURE);
    }

    // Montamos el sistema de ficheros
    bmount(argv[1]);

    // Obtenemos la información
    *buffer = 0;
    mi_dir(argv[2], buffer);

    // Comprobamos si ha ido satisfactoriamente
    switch (errno) {
        case ENOTDIR:
            fprintf(stderr, "ERROR: Debes introducir la ruta de un directorio\n");
            exit(EXIT_FAILURE);
            break;
        case ENOENT:
            fprintf(stderr, "ERROR: El directorio no existe\n");
            exit(EXIT_FAILURE);
            break;
        case EACCES:
            fprintf(stderr, "ERROR: No hay permisos de lectura\n");
            exit(EXIT_FAILURE);
            break;
    }

    printf("%s", buffer);
    bumount();
}
